#!/bin/bash

outpath="./tests/test-data/cmip6output"

declare -a scenarios=("G6solar" "ssp585" "historical" "piControl")
declare -a mips=("GeoMIP" "ScenarioMIP" "CMIP" "CMIP")
declare -a versions=("v20190311" "v20190903" "v20180803" "v20200326")
declare -a tables=("Emon" "fx")
declare -a vars=("cLand" "areacella")

institute="NCAR"
model="CESM2-WACCM"
variant="r1i1p1f1"
# institute="MOHC"
# model="UKESM1-0-LL"
institute="IPSL"
model="IPSL-CM6A-LR"
variant="r1i1p1f1"
grid="gr"

i=0
for scen in "${scenarios[@]}"; do

    echo $scen

    mip=${mips[i]}
    echo $mip

    version=${versions[i]}
    echo $version

    j=0
    for var in "${vars[@]}"; do
        echo $var
        table=${tables[j]}
        echo $table

        outdir="${outpath}/CMIP6/${mip}/${institute}/${model}/${scen}/${variant}/${table}/${var}/${grid}/${version}"
        mkdir -p "${outdir}"
        for f in /data/cmip6/CMIP6/${mip}/${institute}/${model}/${scen}/${variant}/${table}/${var}/${grid}/${version}/*.nc; do

            echo $f
            filename=$(basename -- "$f")
            echo $filename
            cdo -remapbil,n12 $f "${outdir}/${filename}"

        done

        j=$((j+1))
    done

    i=$((i+1))

done
