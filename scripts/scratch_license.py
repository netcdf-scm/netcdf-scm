import glob
import os.path

import tqdm.autonotebook as tqdman
from pymagicc.io import read_mag_file_metadata

DATA_DIR = os.path.join("tests", "test-data", "expected-stitch-output")
# DATA_DIR = os.path.join("/data/cmip6calibrationdata/ascii/mag/monthly/CMIP6")
# DATA_DIR = os.path.join("/data/cmip6calibrationdata/cmip5/ascii/mag/monthly")

print("Entering glob")
for f in tqdman.tqdm(glob.glob(os.path.join(DATA_DIR, "**", "*.MAG"), recursive=True)):
    header = read_mag_file_metadata(f)
    licenses = [v for k, v in header.items() if "license" in k]
    if not licenses:
        print("No license information for {}".format(os.path.basename(f)))
        continue
    # else:
    #     import pdb
    #     pdb.set_trace()
    #     from pprint import pprint
    #     pprint(header)

    non_ccas4 = [
        license
        for license in licenses
        if not (
            ("Creative Commons Attribution" in license)
            and ("ShareAlike 4.0 International License" in license)
            and (
                "for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment"
                in license
            )
        )
    ]
    if non_ccas4:
        import pdb

        pdb.set_trace()
