import os.path

from netcdf_scm.io import load_scmrun


def load_data():
    DATA_PATH = os.path.join(
        "../output-speedtest/netcdf-scm-crunched/CMIP6/CMIP/BCC/BCC-CSM2-MR/piControl/r1i1p1f1/Amon/tas/gn/v20181016/netcdf-scm_tas_Amon_BCC-CSM2-MR_piControl_r1i1p1f1_gn_185001-244912.nc"
    )

    # DATA_PATH = os.path.join("../output-speedtest/netcdf-scm-crunched/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Omon/thetao/gn/v20190313/netcdf-scm_thetao_Omon_CESM2_historical_r10i1p1f1_gn_195310-195312.nc")
    # helper, scm_cubes = _load_helper_and_scm_cubes(DATA_PATH)
    res = load_scmrun(DATA_PATH)
    regions = res.get_unique_meta("region")
    assert len(regions) > 5, regions
