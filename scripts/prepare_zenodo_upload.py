"""
This script is based on two sources.

The first is https://github.com/jhpoelen/zenodo-upload, licensed under a MIT License.

MIT License

Copyright (c) 2019 Jorrit Poelen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

The second is https://gitlab.com/schlauch/zenodo-api-test/-/tree/master

Copyright (c) 2018 German Aerospace Center (DLR). All rights reserved.
SPDX-License-Identifier: MIT-DLR

Valid-License-Identifier: MIT-DLR
License-Text:

MIT License

Copyright (c) 2018 German Aerospace Center (DLR). All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
import argparse
import json
import logging
import os.path
import sys

import requests

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
LOGGER.addHandler(handler)


def get_new_version(deposition_id, zenodo_url, zenodo_key):
    LOGGER.info("Creating new version for deposition id: %s", deposition_id)

    url_to_hit = "{}/api/deposit/depositions/{}/actions/newversion".format(
        zenodo_url, deposition_id
    )

    LOGGER.debug("Posting to: %s", url_to_hit)

    response = requests.post(url_to_hit, params={"access_token": zenodo_key},)
    response.raise_for_status()

    new_version = response.json()["links"]["latest_draft"].split("/")[-1]

    LOGGER.info("New version: {}".format(new_version))

    return new_version


def set_upload_metadata(deposition_id, zenodo_url, deposit_metadata, zenodo_key):
    LOGGER.info("Setting metadata for deposition id: %s", deposition_id)

    url_to_hit = "{}/api/deposit/depositions/{}".format(zenodo_url, deposition_id)

    LOGGER.debug("Sending to: %s", url_to_hit)

    response = requests.put(
        url_to_hit,
        params={"access_token": zenodo_key},
        data=json.dumps(deposit_metadata),
        headers={"Content-Type": "application/json"},
    )
    response.raise_for_status()

    LOGGER.debug("Successfully set metdata")

    return None


def get_bucket(deposition_id, zenodo_url, deposit_metadata, zenodo_key):
    LOGGER.info("Getting bucket for deposition id: %s", deposition_id)

    url_to_hit = "{}/api/deposit/depositions/{}".format(zenodo_url, deposition_id)

    LOGGER.debug("Sending to: %s", url_to_hit)

    headers_bucket_request = {
        "Accept": "application/json",
        # 'Authorization': 'Bearer {}'.format(zenodo_key),
    }

    response = requests.get(
        url_to_hit, headers=headers_bucket_request, params={"access_token": zenodo_key},
    )
    response.raise_for_status()

    bucket = response.json()["links"]["bucket"]
    LOGGER.debug("Successfully retrieved bucket: %s", bucket)

    return bucket


def remove_files(deposition_id, zenodo_url, zenodo_key):
    LOGGER.info("Removing files at deposition id: %s", deposition_id)

    url_to_hit = "{}/api/deposit/depositions/{}/files".format(zenodo_url, deposition_id)

    LOGGER.debug("Sending to: %s", url_to_hit)

    response = requests.get(url_to_hit, params={"access_token": zenodo_key})
    response.raise_for_status()

    for file_entry in response.json():
        LOGGER.info("Removing file: %s", file_entry["id"])

        url_to_hit = "{}/api/deposit/depositions/{}/files/{}".format(
            zenodo_url, deposition_id, file_entry["id"]
        )
        LOGGER.debug("Sending to: %s", url_to_hit)

        response_file = requests.delete(
            url_to_hit, params={"access_token": zenodo_key},
        )
        response_file.raise_for_status()


def upload_file(zenodo_url, deposition_id, upload_file, zenodo_file, deposit_metadata):
    version = "v{}".format(upload_file.split("-")[-1].split(".tar")[0])
    deposit_metadata["metadata"]["version"] = version

    filename = os.path.basename(upload_file)

    LOGGER.info("File to upload: %s", upload_file)
    LOGGER.info("Filename: %s", filename)
    LOGGER.info("Version: %s", version)
    LOGGER.info("Zenodo base url: %s", zenodo_url)
    LOGGER.info("Zenodo deposition id: %s", deposition_id)
    LOGGER.debug("Zenodo file: %s", zenodo_file)
    LOGGER.debug("Deposit metadata: %s", deposit_metadata)

    zenodo_key = open(zenodo_file, "rb").read().strip()

    new_version = get_new_version(deposition_id, zenodo_url, zenodo_key)
    set_upload_metadata(new_version, zenodo_url, deposit_metadata, zenodo_key)
    remove_files(new_version, zenodo_url, zenodo_key)
    bucket = get_bucket(new_version, zenodo_url, deposit_metadata, zenodo_key)

    print(
        "File is ready to upload, execute\n"
        "curl --progress-bar -o /dev/null --upload-file {} {}/{}?access_token=$ZENODO_TOKEN".format(
            upload_file, bucket, filename,
        )
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Prepare zenodo upload")
    parser.add_argument("file", nargs=1, help="File to upload", type=str)

    args = parser.parse_args()

    DEPOSITION_ID = "638907"
    URL = "https://sandbox.zenodo.org"
    ZENODO_FILE = ".zenodo.sandbox"
    # DEPOSITION_ID = "3902225"
    # URL = "https://zenodo.org"
    # ZENODO_FILE = ".zenodo"

    KEYWORDS = [
        v.strip().strip(",").strip('"')
        for v in open("setup.py")
        .read()
        .split("KEYWORDS")[1]
        .split("[")[1]
        .split("]")[0]
        .split("\n")
        if v
    ]
    LICENSE = "BSD-3-Clause"

    DEPOSIT_METADATA = {
        "metadata": {
            "title": "netCDF-SCM",
            "upload_type": "software",
            "description": "Simple wrappers for processing netcdf files for use with simple climate models (https://netcdf-scm.readthedocs.io/en/latest/). If used, please also cite Nicholls, Lewis et al, GDJ 2021 (https://doi.org/10.1002/gdj3.113) as appropriate.",
            "creators": [
                {
                    "name": "Zebedee Nicholls",
                    "affiliation": "Australian-German Climate & Energy College, University of Melbourne",
                },
                {
                    "name": "Jared Lewis",
                    "affiliation": "Australian-German Climate & Energy College, University of Melbourne",
                },
            ],
            "language": "eng",
            "keywords": KEYWORDS,
            "license": LICENSE,
        }
    }

    upload_file(URL, DEPOSITION_ID, args.file[0], ZENODO_FILE, DEPOSIT_METADATA)
