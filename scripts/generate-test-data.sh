#!/bin/bash

BASE_DIR="CMIP6/ScenarioMIP/EC-Earth-Consortium/EC-Earth3/ssp370/r4i1p1f1/Amon/tas/gr/v20200425"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r1i1p1f1/Amon/tas/gr/v20200310"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r11i1p1f1/Amon/tas/gr/v20200201"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r4i1p1f1/Amon/tas/gr/v20200425"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/piControl/r1i1p1f1/Amon/tas/gr/v20200312"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r4i1p1f1/Amon/tas/gr/v20200425"
BASE_DIR="CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3/historical/r4i1p1f1/Amon/tas/gr/v20200425"
BASE_DIR="CMIP6/ScenarioMIP/NIMS-KMA/KACE-1-0-G/ssp370/r1i1p1f1/Amon/tas/gr/v20191217"
BASE_DIR="CMIP6/CMIP/NIMS-KMA/KACE-1-0-G/historical/r1i1p1f1/Amon/tas/gr/v20191028"
BASE_DIR="CMIP6/CMIP/NIMS-KMA/KACE-1-0-G/piControl/r1i1p1f1/Amon/tas/gr/v20191017"
BASE_DIR="CMIP6/CMIP/MPI-M/MPI-ESM1-2-HR/historical/r1i1p1f1/Amon/tas/gn/v20190710"
BASE_DIR="CMIP6/CMIP/MPI-M/MPI-ESM1-2-HR/piControl/r1i1p1f1/Amon/tas/gn/v20190710"
BASE_DIR="CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp245/r1i1p1f1/Amon/tas/gn/v20190710"

COPY_FROM_DIR="/data/cmip6/${BASE_DIR}"
OUT_DIR="./tests/test-data/cmip6output/${BASE_DIR}"

# this fails if there are whitespaces in filenames
for file in $(find "${COPY_FROM_DIR}" -name '*.nc')
do
   echo $file

   outfile="${OUT_DIR}${file//$COPY_FROM_DIR/}"
   echo $outfile

   dir_to_make=$(dirname "${outfile}")
   echo $dir_to_make
   mkdir -p $dir_to_make

   cdo remapbil,r10x10 $file $outfile

   du -sh $dir_to_make
done
