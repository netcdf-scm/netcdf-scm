#!/bin/bash
CRUNCH_DIR="../tests/test-data/cmip6output/CMIP6/CMIP/BCC/BCC-CSM2-MR/piControl/r1i1p1f1/Amon/tas/gn/v20181016"
# CRUNCH_DIR="../tests/test-data/cmip6output/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Omon/thetao"
OUTDIR="../output-speedtest"
mkdir -p "${OUTDIR}"

netcdf-scm --log-level DEBUG crunch \
    "${CRUNCH_DIR}" \
    "${OUTDIR}" \
    test@zeb.com \
    --drs CMIP6Output \
    --regions "World,World|Ocean,World|Northern Hemisphere,World|AR6 regions|WSB,World|AR6 regions|SES,World|AR6 regions|GIC,World|AR6 regions|NWN,World|AR6 regions|SCA,World|AR6 regions|NSA,World|AR6 regions|EEU,World|AR6 regions|ESAF,World|AR6 regions|WSAF,World|AR6 regions|NEAF" \
    --force

python -mtimeit -s'import load_scmdataframe_speedtest' 'load_scmdataframe_speedtest.load_data()'
