#!/bin/bash

declare -a to_copy=(
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/piControl/r1i1p1f2/Amon/tas/gr/v20180814"
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/historical/r1i1p1f2/Amon/tas/gr/v20180917"
    "CMIP6/ScenarioMIP/CNRM-CERFACS/CNRM-CM6-1/ssp245/r1i1p1f2/Amon/tas/gr/v20190219"
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/piControl/r1i1p1f2/Amon/tas/gr/v20180814"
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/historical/r3i1p1f2/Amon/tas/gr/v20190125"
    "CMIP6/ScenarioMIP/CNRM-CERFACS/CNRM-CM6-1/ssp245/r3i1p1f2/Amon/tas/gr/v20190410"
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/piControl/r1i1p1f2/Amon/tas/gr/v20180814"
    "CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/historical/r5i1p1f2/Amon/tas/gr/v20190125"
    "CMIP6/ScenarioMIP/CNRM-CERFACS/CNRM-CM6-1/ssp245/r5i1p1f2/Amon/tas/gr/v20190410"
)

i=0
for f in "${to_copy[@]}"; do

    echo $f

    mkdir -p "${f}"
    rsync -arv cmip6-crunching:"/data/cmip6/${f}/" "${f}/"

done
