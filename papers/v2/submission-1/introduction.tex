Coupled atmosphere-ocean earth system models are our most comprehensive representations of the climate system.
Earth system models from around the globe are currently running as part of the Sixth Coupled Model Intercomparison Project (CMIP6, \citet{eyring_2016_fjksdl}).
CMIP6 builds on the Fifth Coupled Model Intercomparison Project (CMIP5, \citet{Taylor_2012_b7xgw2}), the output of which is still widely used.
Combined, these two projects represent our most comprehensive, physically based estimates of the coupled earth system's behaviour under a wide range of experiments.

However, CMIP5 and CMIP6 models are computationally expensive hence cannot be run for all applications of interest.
To fill this gap, a number of so-called reduced complexity or simple climate models have been developed \citep{nicholls_2020_rcmipphase1}.
An important part of developing such models is calibration: deriving a set of parameters which allows them to best replicate the behaviour of the more complex models which participate in the CMIP experiments.
In order to do this calibration, one must first process the output from CMIP5 and CMIP6.

Once complete, the CMIP6 archive will be one of the world's largest data archives, with an expected total volume in the region of 18PB \citep{balaji_2018_cmipinfra}.
Fortunately, simple climate modellers typically only require annual-mean or monthly model output on hemispheric and land/ocean scales.
This significantly reduces the volume of data they must handle.
Nonetheless, simple climate models typically include a number of different modules which cover the full range of the emissions-climate change cause-effect chain.
Calibrating all of these different modules requires handling several datasets, such that the total volume of interest to a `comprehensive' simple climate model will still be of the order of 50TB.
Processing a data volume of this size is an intimidating task, even for expert users.

The data processing is further complicated by five extra factors.
The first is that the raw data is all in the custom, climate-specific netCDF data format \citep{netcdf_2020}.
Without specialist training, it cannot be read, let alone analysed.
The second factor is that the data is all sorted according to a highly regularised data reference syntax \citep{balaji_2018_cmipinfra}.
Such regularisation is required to make the data able to be processed by machines, however it can be confusing for non-experts.
The third factor is that data is typically presented in absolute values.
However, simple climate models are typically perturbation models i.e. they calculate perturbations from some reference state rather than absolute values.
Given the data volume and sometimes complex relationship between CMIP experiments, calculating such perturbations for a large data volume is not a trivial task.
The fourth factor is licensing.
All CMIP5 and CMIP6 files are released under a specific license which users must adhere to.
The final factor is retractions i.e. removal of data which is later identified as erroneous.
Such retractions are essential to avoid erroneous results propogating into the scientific literature.

Here we present a CMIP5 and CMIP6 derived dataset which is ready for use by reduced complexity climate modellers, derived using the open source tool we developed, netCDF-SCM (netCDF handling for simple or reduced complexity climate modellers, see Section \ref{sub:methods}).
It is the result of addressing all the complications described above and includes global-, hemispheric-, land/ocean annual and monthly means of a variety of CMIP5 and CMIP6 output.
Given the processing performed, this dataset is orders of magnitude smaller than the original data and hence much simpler to work with.
While the dataset is targeted at developers of simple climate models, the reduced data volume means that we can feasibly provide the data in a text-based format.
This also allows non-expert users beyond the climate science community to read and analyse the data as they no longer need to engage with the climate-specific netCDF format.
