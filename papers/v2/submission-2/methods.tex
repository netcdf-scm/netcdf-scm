In principle, our derived dataset is easy to produce.
It consists of weighted means of different horizontal area sections of the raw data, which are then joined together to create timeseries which span both the historical and future scenario simulations.
In practice, the collection, joining and processing of the huge data volumes turns it into a non-trivial effort.

To address this in an automated way, we have developed the netCDF-SCM package \url{gitlab.com/netcdf-scm/netcdf-scm}, available under the open source initiative approved BSD-3-Clause license (see \url{https://opensource.org/licenses} and \citet{Morin2012} for more information on open-source software licenses for scientists).
The netCDF-SCM package builds on the Iris package \citep{Iris}, and relies heavily on the numerous capabilities Iris offers.
We welcome the addition of new features to the netCDF-SCM package (its development is hosted at \url{gitlab.com/netcdf-scm/netcdf-scm}).

We have validated netCDF-SCM in a number of ways.
Firstly, we have compared our global-mean calculations with the limited set of data available on the KNMI climate explorer (\url{climexp.knmi.nl/start.cgi}, last accessed 7\textsuperscript{th} October 2020) and found them to be within 0.1\% in all cases.
Secondly, we have run an extensive test suite over the entire netCDF-SCM package (the test suite is run with every update to the software in a process known as `continuous integration', see e.g. \url{https://about.gitlab.com/blog/2018/01/22/a-beginners-guide-to-continuous-integration/}).
This test suite includes unit tests (tests of individual bits of functionality in isolation from the rest of the package), integration tests (end-to-end tests of the behaviour of the package) and regression tests (tests of changes in outputs of the package compared to previous versions).
Combined, these tests cover 98\% of the codebase, with much of the package being run many times over as part of the testing process.

\subsubsection{Weighted means} % (fold)
\label{sub:weighted_means}

We combine raw data, cell areas and cell surface fraction information to produce appropriately weighted means for each region of interest (figure \ref{fig:workflow}).
We calculate the weighted means as follows:

\begin{equation} \label{eqn:weighted-means}
    v(r, t, \vec{Z}) = \frac{\sum_{\vec{Y}} X(t, \vec{Y}, \vec{Z}) \times A(\vec{Y}, \vec{Z}) \times f(r, \vec{Y}, \vec{Z})}{\sum_{\vec{Y}} A(\vec{Y}, \vec{Z}) \times f(r, \vec{Y}, \vec{Z})}
\end{equation}
where $v(r, t, \vec{Z})$ is the output for horizontal region of interest $r$ at time $t$ and non-horizontal spatial co-ordinate vector $\vec{Z}$, $\vec{Y}$ is the horizontal spatial co-ordinate of each cell, $X(t, \vec{Y}, \vec{Z})$ is the raw data at time $t$ in the cell with horizontal spatial co-ordinate vector $\vec{Y}$ and non-horizontal spatial co-ordinate vector $\vec{Z}$, $A(\vec{Y}, \vec{Z})$ is the horizontal area of the cell, and $f(r, \vec{Y}, \vec{Z})$ is the relevant surface fraction of the cell for the region of interest.
Here `horizontal region' refers to a region defined by latitude and longitude only.

The cell area and surface fractions specific to the climate model of interest are determined using the CMIP data reference syntax and the CMIP output metadata.
If the cell area and/or surface fraction information is not available, netCDF-SCM will simply use inbuilt best-guess cell areas and surface fractions.
The best-guesses cell areas use Iris' \citep{Iris} functionality to calculate cell areas based on each cell's bounds (cell areas are calculated as $r^2 (lon_1 - lon_0) * (\sin(lat_1) - \sin(lat_0))$, where $r$ is the Earth's radius, $lon_1$ and $lon_0$ are the longitude (east-west) bounds of the cell and $lat_1$ and $lat_0$ are the latitude (north-south) bounds of the cell).
The best-guess surface fractions are the result of interpolating the surface fractions from \emph{IPSL-CM6A-LR} climate model historical simulations \citep{cmip6data_CMIP6_CMIP_IPSL_IPSL-CM6A-LR_historical} onto a regular 1\textdegree x 1\textdegree grid.
When calculating weighted averages, these best-guess surface fractions are then interpolated onto the model of interest's grid.
This process works best with data on a regular grid.
Given their complexity, on some models' native grids it may be necessary to obtain the cell area and surface fraction information before the data can be processed.
We have examined the importance of the choice of best-guess surface fractions and found that basing our best-guess surface fractions on other climate models would make a negligible difference (less than 1\%) to our results.
In cases where the cell area and surface fraction are particularly important (e.g. for small regions), we stress that the most accurate results can only be obtained by using the cell area and surface fractions specific to the climate model of interest.

We also ensure that we use the surface fractions appropriate to the domain of the data of interest.
For atmospheric data, we simply use surface land fractions for land regions and surface ocean fractions for ocean regions.
For land data, e.g. data from C4MIP \citep{jones_c4mip_2016}, we use surface land fractions.
These surface fraction weights are important to apply because `you do need to weight the output by land frac (sftlf is the CMIP variable name)' \citep{jones_2020_personalcomm} in order to calculate weighted means correctly.
In contrast, for ocean data, we do not use surface ocean fractions.
The reason is that the data is given with respect to the horizontal area of the entire cell, not the horizontal area of ocean in the cell \citep{griffies_2016_omip}.

This step is the most computationally expensive step.
Being built on Iris \citep{Iris}, netCDF-SCM is able to handle large datasets, largely thanks to the dask package \citep{dask_2016}.
With parallel processing and user-defined memory usage settings, netCDF-SCM is able to be run on personal computers as well as cloud high performance computing infrastructures.

% subsection weighted_means (end)

\subsubsection{Stitching experiments} % (fold)
\label{sub:joining_experiments}

CMIP data typically comes in a `family' of experiments, with each experiment having a `parent' experiment and potentially `children', `grandchildren', `great-grandchildren' etc.
Each `generation' can be joined with the previous to make a longer, continuous timeseries.

An obvious example of this is the ScenarioMIP \citep{ONeill_2016_f87n56} experiments.
ScenarioMIP includes simulations of future scenarios.
In order to create a complete timeseries from pre-industrial times through to the future, the scenario simulations must be joined with the historical simulations (their parent experiment) and the pre-industrial control (piControl) simulations.
The need for joining experiments in this way, or `stitching' them, appears beyond scenario simulations too.
For example, many ZECMIP \citep{jones_2019_976cds} experiments are the children of the one percent per year increase in atmospheric CO\textsubscript{2} concentration experiments (1pctCO2).

Stitching experiments together back to pre-industrial control runs requires a number of steps (Algorithm \ref{alg:stitching}).
It is necessary to combine the metadata provided in each file with the data reference syntax to efficiently traverse the data archive to find each relevant output.
Then the branch times in each experiment must be checked and aligned to ensure that the continuous timeseries is as intended.
For full provenance, the metadata from each generation which has contributed to the `stitched' output should also be preserved.
Performing these steps is another one of netCDF-SCM's key functions, leading to continuous stitched outputs with complete metadata of all datasets which have been used to make that output.

The final component of joining timeseries is de-drifting or applying `normalisation'.
In this context, normalisation refers to calculating anomalies from some reference values.
In some cases this is trivial.
For example, taking anomalies from a given period within the existing output e.g. calculating anomalies relative to the 1850-1900 period.

However, there are many cases which require more complex analyses.
For example, calculating anomalies relative to a 21-year (or 30-year) running mean of the equivalent period in the pre-industrial control run.
Such a calculation requires finding the pre-industrial control data, identifying the equivalent period in the pre-industrial control run, correctly lining it up with the data to be normalised before finally calculating the anomalies.
This can be done for single ensemble members (Figure \ref{fig:stitching-and-normalisation}) and for multiple members (in which case particular care must be taken to normalise each ensemble member against the correct period from the pre-industrial control run, Supplementary Figure S1).
For one off cases this can be done manually, however automated solutions are necessary to perform it over an entire CMIP archive.

Within our dataset, we currently offer four different normalisation options.
Before describing these options, we stress that all of our processing data is openly available so users who wish to use different normalisation options to the ones provided are able to do so.
Our four different normalisation options are anomalies and de-drifting with 21- and 30-year running means as reference values.
The anomalies are calculated as the difference between the model output and the corresponding running mean from the pre-industrial control experiment.
In constrast, the de-drifting calculations simply remove any drift in the running mean of the pre-industrial control experiment, without calculating differences.
The anomaly calculations are useful for variables such as \emph{tas} (surface air temperature), where changes in the variable from the pre-industrial state are of most interest.
The de-drifting calculations are use for variables such as \emph{cLand} (total carbon in all terrestrial pools), where the absolute values are of importance but it is also important to remove model drift before performing further analysis.

For all normalisation options, we use an equation of the form:

\begin{equation} \label{eqn:normalisation}
    v_{\text{norm}}(r, t) = v_{\text{raw}}(r, t) - v_{\text{pi}}(r, t)
\end{equation}
where $v_{\text{norm}}(r, t)$ is the normalised values for region $r$ at time $t$, $v_{\text{raw}}(r, t)$ is the raw values and is $v_{\text{pi}}(r, t)$ the running-mean reference values from the pre-industrial control run (either absolute values or drift values depending on the normalisation method).

\begin{algorithm}
\caption{Algorithm for stitching and normalising. This algorithm does two things. Firstly, it joins together experiments which form a continuous sequence (e.g. a scenario based experiment which continues from a historical experiment). Secondly, it can, if desired, normalise experiments against pre-industrial control values. This allows users to e.g. calculate deviations from the background state or remove model drift from their output timeseries.}
\label{alg:stitching}
\begin{algorithmic}
\STATE load data
\REPEAT
    \STATE find parent data
    \STATE align branch times in child and parent
    \STATE stitch onto existing data
    \STATE update metadata to keep track of ancestry
\UNTIL{parent is piControl or esm-piControl}

\IF{data should be normalised}
    \STATE determine branch time in pre-industrial control run
    \STATE calculate normalisation from pre-industrial control run (using method specified by the user)
    \STATE normalise data
    \RETURN stitched and normalised data
\ELSE
    \RETURN stitched data
\ENDIF
\end{algorithmic}
\end{algorithm}

% subsection joining_experiments (end)

\subsubsection{Retracted data} % (fold)
\label{sub:retracted_data}

CMIP data is occasionally found to be erroneous and hence retracted \citep{balaji_2018_cmipinfra}.
To handle this, as part of netCDF-SCM, we provide a simple tool which checks if a data file is based on any retracted data (see \url{https://netcdf-scm.readthedocs.io/en/latest/usage/using-cmip-data.html}, last accessed June 25 2020).
In addition, the tool will also examine the data's license and, to the extent possible, warn the user about any non-standard license terms.

These tools take advantage of CMIP6's `dataset-centric rather than systemcentric' approach \citep{balaji_2018_cmipinfra}.
The dataset-centric approach allows data users to check the validity of their data at point of use, rather than relying on the data provider to have done this for them.
The dataset-centric approach also ensures that `dark repositories' \citep{balaji_2018_cmipinfra}, such as our derived dataset, maintain the connection between data user and the original source of the data.
In our derived dataset, we maintain this connection by providing the persistent identifiers of all datasets within our metadata (specifically the \verb|tracking_id| attributes).

% subsection redacted_data (end)
