The dataset described in this paper is openly available at \url{https://doi.org/10.5281/zenodo.3951890} and all our data processing code is openly available at \url{https://gitlab.com/netcdf-scm/calibration-data}.
Any use of the data must follow CMIP's terms of use (see discussion in Section \ref{sub:datasets}).
At present, our dataset contains timeseries for over 100 models and 40 experiments of interest from the CMIP5 and CMIP6 archives.
In total, we have over 40 000 timeseries.
To date, we have processed 83 variables, full descriptions of which are available in the `Model output specifications' section of \url{https://pcmdi.llnl.gov/CMIP6/Guide/dataUsers.html} (last accessed 8\textsuperscript{th} October 2020).
Our full variable list is: energy flux and temperature variables - \emph{hfds}, \emph{rlut}, \emph{rsdt}, \emph{rsut}, \emph{tas}, \emph{tos}, \emph{ts}; carbon cycle related variables - \emph{cLand}, \emph{cLitter}, \emph{cLitterGrass}, \emph{cLitterShrub}, \emph{cLitterSubSurf}, \emph{cLitterSurf}, \emph{cLitterTree}, \emph{cMisc}, \emph{cOther}, \emph{cProduct}, \emph{cSoil}, \emph{cSoilFast}, \emph{cSoilMedium}, \emph{cSoilSlow}, \emph{cStem}, \emph{cVeg}, \emph{cVegGrass}, \emph{cVegShrub}, \emph{cVegTree}, \emph{cWood}, \emph{co2mass}, \emph{co2s}, \emph{fAnthDisturb}, \emph{fBNF}, \emph{fDeforestToAtmos}, \emph{fDeforestToProduct}, \emph{fFire}, \emph{fFireAll}, \emph{fFireNat}, \emph{fGrazing}, \emph{fHarvest}, \emph{fHarvestToAtmos}, \emph{fHarvestToProduct}, \emph{fLitterSoil}, \emph{fLuc}, \emph{fNAnthDisturb}, \emph{fNLitterSoil}, \emph{fNProduct}, \emph{fNVegLitter}, \emph{fNdep}, \emph{fNfert}, \emph{fNgas}, \emph{fNgasFire}, \emph{fNgasNonFire}, \emph{fNloss}, \emph{fNnetmin}, \emph{fNup}, \emph{fVegLitter}, \emph{fco2antt}, \emph{fco2fos}, \emph{fco2nat}, \emph{fgco2}, \emph{gpp}, \emph{nbp}, \emph{nep}, \emph{netAtmosLandCO2Flux}, \emph{npp}, \emph{nppGrass}, \emph{nppOther}, \emph{nppShrub}, \emph{nppTree}, \emph{ra}, \emph{rh}, \emph{rhGrass}, \emph{rhLitter}, \emph{rhShrub}, \emph{rhSoil}, \emph{rhTree}; nitrogen cycle related variables - \emph{nLand}, \emph{nLeaf}, \emph{nLitter}, \emph{nMineral}, \emph{nProduct}, \emph{nRoot}, \emph{nSoil}, \emph{nStem}.
We include output for 11 large-scale regions (World, Northern Hemisphere, Southern Hemisphere, ocean, land, Northern Hemisphere ocean, Northern Hemisphere land, Southern Hemisphere ocean, Southern Hemisphere land, North Atlantic Ocean and the El Ni\~{n}o 3.4 region, defined as the region within 5\textdegree N - 5\textdegree S and 170\textdegree W - 120\textdegree W) and, by using the regionmask package \citep{hauser_2020_regionmask}, all of the IPCC climate reference regions defined by \citet{iturbide_2020_ipccregions}.

The timeseries are continuous, monthly timeseries for specific variables, experiments and regions of interest for a selection of the CMIP5 and CMIP6 archives.
These timeseries are the combination of the experiment of interest, any (potentially multiple) experiments from which it `branched' and any normalisation which is applied (Section \ref{sub:methods} and Figure \ref{fig:stitching-and-normalisation}).

Our dataset is a different product compared to the data available in the IPCC AR6 Atlas (\url{https://github.com/SantanderMetGroup/ATLAS}, last accessed 9\textsuperscript{th} October 2020).
At present, the only similarity is that both our dataset and the Atlas provide surface air temperature (\emph{tas}) at various regional aggregations for tier 1 experiments from CMIP5 and CMIP6.
However, while the Atlas uses a binary land/ocean mask (i.e. each value is a one or a zero) and cosine of latitude as a proxy for area weights, we use the model reported cell areas (where available) and a continuous land fraction (including subtleties for land-only and ocean-only data, see Section \ref{sub:weighted_means}) to calculate weighted, aggregate metrics.
Secondly, we provided stitched outputs, joining each experiment with its parent, grandparent etc. experiments.
Thirdly, the Atlas provides precipitation (\emph{pr}) timeseries, whereas we do not provide precipitation.
Instead, we provide data for 82 other variables as described previously.
Finally, the Atlas provides one ensemble member per climate model while we provide as many ensemble members as are available.

We provide our data in a comma separated value (csv) format.
This format is composed of three key parts and uses the extension \verb|.MAG| because it is directly compatible with the MAGICC7 reduced complexity climate model \citep{meinshausen_2020_ssps}.
The first part is the header, which contains the date the file was written, the contact for the file, the version of netCDF-SCM used to crunch the data and the version of Pymagicc \citep{Gieseke2018pymagicc} used to write the file.
The second is the metadata.
This contains all metadata from each of the raw datafiles, plus extra information and metadata about the method used to derive the final timeseries included in the file.
It also contains a FORTRAN90 namelist with basic information about the data in the file.
A particularly useful bit of information is the \verb|THISFILE_FIRSTDATAROW| line, which allows automated readers to skip to the line of interest if they are only interested in the data.
The third and final section is the data.
The data block is composed of a four-line header with variable, units and region information for each timeseries as well as a MAGICC7 specific row, \verb|TODO|, which can generally be ignored.
After the header comes the data itself.
The data block has column-oriented data, with the first column being the time axis (in years) and each subsequent column being a different timeseries (sometimes referred to as `wide' data although this term is imprecise \citep{wickham_2014_tidydata}).

The data archive grows as we add new CMIP6 results.
An up to date full collection (alongside instructions for automated downloads) can be found at \url{cmip6.science.unimelb.edu.au}.
Examples of how to use the data can be found in \url{https://gitlab.com/netcdf-scm/calibration-data/-/tree/master/notebooks} (last accessed 25 June 2020) and we encourage any users of the data to add further examples, especially in computing languages other than Python.
