The key users of this dataset are reduced complexity climate modellers.
These regional-aggregate timeseries are a key part of model calibration (see e.g. \cite{Meinshausen2011}) and comprehensive datasets allow reduced complexity models to be validated over a wide range of experiments and output variables.

Having said this, we believe that the dataset can be useful well-beyond the reduced complexity climate model community.
As discussed in Section \ref{sec:introduction}, processing CMIP data is an intimidating task for expert users and not possible for those without specialist training.
We hope that our aggregate dataset removes this need for specialist training, thanks to its significantly reduced data volume and text-based format.
As a result, the dataset presented here may be useful climate change researchers outside the climate modelling community, policy makers, businesses and even journalists.

The dataset presented here comes with three important caveats.
The first is that we make no guarantees about how up-to-date our data is.
As discussed previously, the onus is on users of the data to check for retractions before using the data (see Section \ref{sub:retracted_data} for discussion of our automated tool for checking such retractions).
The second is that the area-weighting used (Equation \ref{eqn:weighted-means}) is only one of many possible area-weighting choices.
For example, other users may wish to partition data into area/land boxes based on whether the fraction of each gridbox is above some threshold or not.
At present, we do not provide data for area-weighting choices beyond the one described in this paper.
For users who need to do such analysis, we are able to provide guidance on how this could be done with netCDF-SCM via netCDF-SCM's issue tracker (\url{https://gitlab.com/netcdf-scm/netcdf-scm/-/issues}).
Thirdly, we provide only a limited number of ways of calculating anomalies.
Again, for users who wish to calculate anomalies in a different way to what we have provided, netCDF-SCM's issue tracker can be used for discussions and guidance.

On the software side, the netCDF-SCM tool is in its relative infancy and is currently only developed by a limited community.
As a result, many improvements could be made.
We hope that netCDF-SCM's open-source nature, with its extensive tests, invites contributions from throughout the climate community and beyond.
Such contributions will improve netCDF-SCM's functionality and reduces the need for duplicate effort.

As a first suggestion, we note that much of netCDF-SCM's functionality is a duplication of functionality within the ESMValTool, `A community diagnostic and performance metrics tool for routine evaluation of Earth system models in CMIP' \citep{eyring_2016_esmvaltool}.
The duplication results from the parallel development of these two projects, which were both too immature to be combined when they were begun.
Adding netCDF-SCM's functionality into the ESMValTool, which is a much bigger and better supported project, would reduce this duplication and likely provide benefits for both groups.

Outside of integration with the ESMValTool, improvements could be made to netCDF-SCM's memory usage, dask usage and parallelisation.
These may lead to significant processing performance as such optimisations, particularly the use of dask's task planning capabilities, have only been performed to a limited degree to date.
