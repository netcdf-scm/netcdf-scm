Coupled atmosphere-ocean earth system models are our most comprehensive representations of the climate system.
Earth system models from around the globe are currently running as part of the Sixth Coupled Model Intercomparison Project (CMIP6, \citet{eyring_2016_fjksdl}).
CMIP6 builds on the Fifth Coupled Model Intercomparison Project (CMIP5, \citet{Taylor_2012_b7xgw2}), the output of which is still widely used.
Combined, these two projects represent our most comprehensive, physically based estimates of the coupled earth system's behaviour under a wide range of experiments.

However, CMIP5 and CMIP6 models are computationally expensive hence cannot be run for all applications of interest.
To fill this gap, a number of so-called reduced complexity climate models (also referred to as `simple climate models') have been developed \citep{nicholls_2020_rcmipphase1}.
An important part of developing such models is calibration: deriving a set of parameters which allows them to best replicate the behaviour of the more complex models which participate in the CMIP experiments.
In order to do this calibration, one must first process the output from CMIP5 and CMIP6.

Once complete, the CMIP6 archive will be one of the world's largest data archives, with an expected total volume in the region of 18PB \citep{balaji_2018_cmipinfra}.
Fortunately, reduced complexity climate modellers typically only require annual-mean or monthly model output on hemispheric and land/ocean scales.
This significantly reduces the volume of data they must handle.
Nonetheless, reduced complexity climate models typically include a number of different modules which cover the full range of the emissions-climate change cause-effect chain.
Calibrating all of these different modules requires handling several datasets, such that the total volume of raw CMIP output of interest to a `comprehensive' reduced complexity climate model will still be of the order of 50TB.
Processing a data volume of this size is an intimidating task, even for expert users.

The data processing is further complicated by five extra factors.
The first is that the raw data is all in the custom, climate-specific netCDF data format \citep{netcdf_2020}.
Without specialist training, it cannot be read, let alone analysed.
The second factor is that the data is all sorted according to a highly regularised data reference syntax \citep{balaji_2018_cmipinfra}.
Such regularisation is required to make the data able to be processed by machines, however it can be confusing for non-experts.
The third factor is that data is typically presented in absolute values.
However, reduced complexity climate models are typically perturbation models i.e. they calculate perturbations from some reference state rather than absolute values.
Given the data volume and sometimes complex relationship between CMIP experiments, calculating such perturbations for a large data volume is not a trivial task.
The fourth factor is licensing.
All CMIP5 and CMIP6 files are released under a specific license which users must adhere to, and retrieving this information is not easily done.
The final factor is retractions i.e. removal of data which is later identified as erroneous.
Such retractions are essential to avoid erroneous results propogating into the scientific literature.
However, at the moment there are only a limited amount of tools which allow a user to check whether they are using a retracted dataset or not.

The target audience for this dataset is reduced complexity climate models and modellers.
Here, reduced complexity models refers to models which focus on global- and annual-mean properties of the climate system.
As a result of their limited spatial and temporal resolution, reduced complexity models are very computationally efficient.
These models are typically used when more complex models, such as those participating in CMIP, are too computationally expensive to use.
For example, reduced complexity models are used within many integrated assessment models to assess the climate implications of different emissions pathways (given that integrated assessment models typically require hundreds to thousands of climate realisations, using CMIP models is not computationally feasible).
Some prominent examples of reduced complexity models are MAGICC \citep{Meinshausen2011}, FaIR \citep{Smith_2018_fair13} and hector \citep{Hartin2015}.
A detailed discussion of reduced complexity models and an overview of models available in the literature can be found in the first phase of the Reduced Complexity Model Intercomparison Project \citep{nicholls_2020_rcmipphase1}.

Our CMIP5 and CMIP6 derived dataset was extracted using the open source tool we developed, netCDF-SCM (netCDF handling for reduced complexity/simple climate modellers, see Section \ref{sub:methods}) and is ready for use by reduced complexity climate modellers.
It is the result of addressing all the complications described above and includes global-, hemispheric-, land/ocean annual and monthly means of a variety of CMIP5 and CMIP6 output.
Given the processing performed, this dataset is orders of magnitude smaller than the original data.
The reduced data volume means that we can feasibly provide the data in a text-based format.
Thus, while the dataset is targeted at developers of reduced complexity climate models, its simple, text-based format also allows non-expert users beyond the climate science community to read and analyse the data as they no longer need to engage with the climate-specific netCDF format.
