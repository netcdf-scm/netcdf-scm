import argparse

import pandas as pd


pd.set_option('display.max_colwidth', None)


def clean_table(inp, output, mip):
    start = pd.read_csv(inp)
    idx = ["Modelling Group", "Climate Model", "Scenario"]
    start = start.sort_values(by=idx).set_index(idx)
    out_s = start.to_latex(escape=False)

    replacements = (
        ("tabular", "longtable"),
        ("llll", "lllp{8cm}"),
        ("toprule", "hline"),
        ("midrule", "hline"),
        ("bottomrule", "hline"),
    )
    for old, new in replacements:
        out_s = out_s.replace(old, new)


    out_s = out_s.split("\n")[5:-3]
    # out_s.insert(0, r"\begin{supertabular}{lllp{8cm}}")

    header = r"""\begin{center}
\begin{longtable}{lllp{4cm}}
\caption{MIP_ID data used in this study.}
\label{tab:mip-id-lower-data} \\

\hline
      &        &        &    Reference \\
      Modelling Group & Climate Model & Scenario &  \\
\hline
\endfirsthead

\hline
\multicolumn{4}{l}{\small\sl continued from previous page} \\
\hline
\endhead

\hline
\multicolumn{4}{r}{\small\sl continued on next page} \\
\hline
\endfoot

\hline
\endlastfoot
""".replace(
        "MIP_ID", mip
    ).replace(
        "mip-id-lower", mip.lower()
    ).split("\n")

    out_s = header + [""] + out_s

    out_s.append(r"\end{longtable}")
    out_s.append(r"\end{center}")

    with open(output, "w") as fh:
        fh.write("\n".join(out_s))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clean and split latex table into pages")
    parser.add_argument("table", help="Table to clean and split", type=str)
    parser.add_argument("output", help="Output file", type=str)
    parser.add_argument("mip", help="MIP era", type=str)

    args = parser.parse_args()

    clean_table(args.table, args.output, args.mip)
