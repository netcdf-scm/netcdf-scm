To the Editors of Geoscience Data Journal,

Output from the CMIP6 project is currently being added rapidly to the Earth System Grid Federation.
This dataset is one of the biggest scientific datasets available, expected to reach in the order of 20PB once complete.

In this paper we present a dataset dervied from the CMIP6 dataset, specifically monthly mean, global, land/ocean and hemispheric regional aggregations.
The derived dataset is aimed at reduced complexity climate modellers, but may be useful in a variety of fields because it requires no specialist knowledge to use.
For comparison, we also process a limited subset of the CMIP5 archive.

We have processed all our data using open-source software (tool available at gitlab.com/netcdf-scm/netcdf-scm and processing scripts available at gitlab.com/netcdf-scm/calibration-data) and make all our data freely accessible (subject to the CMIP5 and CMIP6 terms of use) at cmip6.science.unimelb.edu.au.

We feel that this paper would be well suited to Geoscience Data Journal and hope that the manuscript meets your high quality criteria.

Best regards,

Zebedee Nicholls (corresponding author)
