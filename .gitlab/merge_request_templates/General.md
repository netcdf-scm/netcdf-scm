# Pull request

Please confirm that this pull request has done the following:

- [ ] Tests added
- [ ] Documentation added (where applicable)
- [ ] Example added (either to an existing notebook or as a new notebook, where applicable)
- [ ] Description in ``CHANGELOG.rst`` added

## Adding to CHANGELOG.rst

Please add a single line in the changelog notes similar to one of the following:

```
- (`!XX <https://gitlab.com/netcdf-scm/netcdf-scm/merge_requests/XX>`_) Added feature which does something
- (`!XX <https://gitlab.com/netcdf-scm/netcdf-scm/merge_requests/XX>`_) Fixed bug identified in (`#YY <https://gitlab.com/netcdf-scm/netcdf-scm/issues/YY>`_)
```
