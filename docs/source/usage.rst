.. usage:

Usage
=====

Here we provide various examples of netCDF-SCM's behaviour and usage.
The source code of these usage examples is available in the folder
`docs/source/usage`_ of the `GitLab repository`_.

.. _`docs/source/usage`:
   https://gitlab.com/netcdf-scm/netcdf-scm/tree/master/doc/source/usage

.. _`GitLab repository`:
   https://gitlab.com/netcdf-scm/netcdf-scm

Basic demos
+++++++++++

.. toctree::
   :maxdepth: 1

   usage/demo.ipynb

CMIP-related usage
++++++++++++++++++

.. toctree::
   :maxdepth: 1

   usage/using-cmip-data.ipynb
   usage/crunch-cmip5-data.ipynb
   usage/cmip-drs-handling.ipynb
   usage/stitching-and-normalisation.ipynb

More detail
+++++++++++

.. toctree::
   :maxdepth: 1

   usage/atmos-land-ocean-handling.ipynb
   usage/ocean-data.ipynb
   usage/wranglers.ipynb
   usage/weights.ipynb
   usage/default-land-ocean-mask.ipynb

Miscellaneous
+++++++++++++

.. toctree::
   :maxdepth: 1

   usage/year-zero-handling.ipynb
   usage/miscellaneous-reading.ipynb
