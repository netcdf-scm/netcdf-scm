.. _cli-reference:

Command-line interface
----------------------

.. click:: netcdf_scm.cli:cli
   :prog: netcdf-scm
   :show-nested:
