.. _normalisation-reference:

Normalisation API
-----------------

.. automodule:: netcdf_scm.normalisation

Base API
========

.. automodule:: netcdf_scm.normalisation.base

After branch time mean API
==========================

.. automodule:: netcdf_scm.normalisation.after_branch_time_mean

Running mean API
================

.. automodule:: netcdf_scm.normalisation.running_mean

Running mean de-drift API
=========================

.. automodule:: netcdf_scm.normalisation.running_mean_dedrift
