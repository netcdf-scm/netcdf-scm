.. netcdf-scm documentation master file, created by
   sphinx-quickstart on Fri Oct 12 23:11:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

netCDF-SCM
==========

.. include:: ../../README.rst
    :start-after: sec-begin-index
    :end-before: sec-end-index

.. include:: ../../README.rst
    :start-after: sec-begin-license
    :end-before: sec-end-license

.. include:: disclaimer.rst

.. toctree::
    :maxdepth: 2
    :caption: Documentation

    installation
    usage
    development

.. toctree::
    :maxdepth: 2
    :caption: API reference

    iris-cube-wrappers
    weights
    citing
    cli
    crunching
    definitions
    errors
    io
    misc-readers
    normalisation
    output
    retractions
    stitching
    utils
    wranglers
    wrangling

.. toctree::
    :maxdepth: 2
    :caption: Versions

    changelog

Index
-----

- :ref:`genindex`
- :ref:`modindex`
- :ref:`search`
