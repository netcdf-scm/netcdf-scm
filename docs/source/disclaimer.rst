Disclaimer
----------

One of the most common uses of netCDF-SCM is for processing `Coupled Model Intercomparison Project <https://www.wcrp-climate.org/wgcm-cmip>`_ data.
If this is your use case, please note that you **must** abide by the terms of use of the data, in particular the required acknowledgement statements (see the `CMIP5 terms of use <https://pcmdi.llnl.gov/mips/cmip5/terms-of-use.html>`_, `CMIP6 terms of use <https://pcmdi.llnl.gov/CMIP6/TermsOfUse/TermsOfUse6-1.html>`_ and `CMIP6 GMD Special Issue <https://www.geosci-model-dev.net/special_issue590.html>`_).

To make it easier to do this, we have developed some basic tools which simplify the process of checking model license terms and creating the tables required in publications to cite CMIP data (`check them out here <usage/using-cmip-data.ipynb>`_).
However, we provide no guarantees that these tools are up to date so all users should double check that they do in fact produce output consistent with the terms of use referenced above (and if there are issues, please raise an issue at our `issue tracker <https://gitlab.com/netcdf-scm/netcdf-scm/issues>`_ :) ).
