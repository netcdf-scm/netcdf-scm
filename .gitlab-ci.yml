stages:
  - build_docker
  - test
  - coverage
  - test_install

image: registry.gitlab.com/netcdf-scm/netcdf-scm:$CI_COMMIT_REF_SLUG


# Build an image which contains all of the build dependencies preinstalled
build:docker:
  stage: build_docker
  except:
    - schedules
  image: docker:latest
  services:
    - docker:dind
  only:
    changes:
      - .dockerignore
      - .gitlab-ci.yml
      - Dockerfile
      - environment.yml
      - MANIFEST.in
      - Makefile
      - setup.py
  variables:
    IMAGE: registry.gitlab.com/netcdf-scm/netcdf-scm:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker build -t $IMAGE -f Dockerfile .
    - docker push $IMAGE

test:unit:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  artifacts:
    paths:
      - .coverage_unit
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list
    - COVERAGE_FILE=.coverage_unit pytest tests/unit/ -r a -v --cov --cov-fail-under=0 --cov-report=

test:integration:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  artifacts:
    paths:
      - .coverage_integration
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list --explicit
    - COVERAGE_FILE=.coverage_integration pytest tests/integration/ -r a -n 4  -v --cov --cov-fail-under=0 --cov-report=

test:regression:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  artifacts:
    paths:
      - .coverage_regression
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list --explicit
    - COVERAGE_FILE=.coverage_regression pytest tests/regression/ -r a -v -n 4 --cov --cov-fail-under=0 --cov-report=

test:scratch_script:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list
    - bash scripts/scratch-process.sh

test:notebooks:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list
    - pytest -rfsxEX --nbval ./docs/source/usage --sanitize ./tests/nbval.cfg

test:format:
  stage: test
  except:
    - schedules
  dependencies:
    - build:docker
  script:
    - source ~/.bashrc
    - pip install -e .
    - conda list
    - flake8 setup.py scripts src tests docs/source/conf.py
    - black --check --exclude _version.py --target-version py37 src tests docs/source/conf.py
    - isort --check-only --quiet src tests setup.py
    - black-nb --check ./docs/source/usage
    - pydocstyle src

coverage:
  stage: coverage
  except:
    - schedules
  dependencies:
    - test:unit
    - test:integration
    - test:regression
  script:
    - source ~/.bashrc
    - pip install coverage
    - ls -al
    - coverage combine .coverage_*
    - coverage report --rcfile=.coveragerc

test_install:conda:
  stage: test_install
  only:
    - schedules
  image: continuumio/miniconda3:latest
  script:
    - conda install pip
    - conda install -c conda-forge 'netcdf-scm==2.0.0*' -y
    - conda list
    - python scripts/test_install_iris_installed.py
