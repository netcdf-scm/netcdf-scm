---- HEADER ----

Date: 2021-02-22 07:34:38
Contact: cmip6output stitching with 31-yr-mean-after-branch-time normalisation regression test
Source data crunched with: netCDF-SCM v2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
File written with: pymagicc v2.0.0 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

(child) CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
(child) CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
(child) Conventions: CF-1.7
(child) area_world (m**2): 509194858320979.25
(child) area_world_el_nino_n3.4 (m**2): 9499544514719.764
(child) area_world_land (m**2): 146750573712803.38
(child) area_world_north_atlantic_ocean (m**2): 39884940686492.164
(child) area_world_northern_hemisphere (m**2): 254597429160489.66
(child) area_world_northern_hemisphere_land (m**2): 98924840662008.36
(child) area_world_northern_hemisphere_ocean (m**2): 155672588435179.16
(child) area_world_ocean (m**2): 362444284572689.1
(child) area_world_southern_hemisphere (m**2): 254597429160489.62
(child) area_world_southern_hemisphere_land (m**2): 47825733050795.016
(child) area_world_southern_hemisphere_ocean (m**2): 206771696137510.0
(child) branch_method: standard
(child) branch_time_in_child: 735110.0
(child) branch_time_in_parent: 735110.0
(child) calendar: 365_day
(child) case_id: 1504
(child) cesm_casename: b.e21.BSSP370cmip6.f09_g17.CMIP6-SSP3-7.0.001
(child) comment: TREFHT
(child) contact: cesm_cmip6@ucar.edu
(child) crunch_contact: cmip6output crunching regression test
(child) crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
(child) crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Land", "World|Ocean", "World|Northern Hemisphere|Land", "World|Southern Hemisphere|Land", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
(child) crunch_source_files: Files: ['/CMIP6/ScenarioMIP/NCAR/CESM2/ssp370/r1i1p1f1/Amon/tas/gn/v20190730/tas_Amon_CESM2_ssp370_r1i1p1f1_gn_201501-206412.nc', '/CMIP6/ScenarioMIP/NCAR/CESM2/ssp370/r1i1p1f1/Amon/tas/gn/v20190730/tas_Amon_CESM2_ssp370_r1i1p1f1_gn_206501-210012.nc']
(child) data_specs_version: 01.00.30
(child) description: near-surface (usually, 2 meter) air temperature
(child) experiment: Gap: Baseline scenario with a medium to high radiative forcing by the end of century. Following approximately RCP7.0 global forcing pathway with SSP3 socioeconomic conditions. Radiative forcing reaches a level of 7.0 W/m2 in 2100.  Concentration-driven.
(child) experiment_id: ssp370
(child) external_variables: areacella
(child) frequency: mon
(child) further_info_url: https://furtherinfo.es-doc.org/CMIP6.NCAR.CESM2.ssp370.none.r1i1p1f1
(child) grid: native 0.9x1.25 finite volume grid (192x288 latxlon)
(child) grid_label: gn
(child) id: tas
(child) institution: National Center for Atmospheric Research
(child) institution_id: NCAR
(child) land_fraction: 0.2882012088588231
(child) land_fraction_northern_hemisphere: 0.3885539653255865
(child) land_fraction_southern_hemisphere: 0.18784845239205966
(child) license: CMIP6 model data produced by <The National Center for Atmospheric Research> is licensed under a Creative Commons Attribution-[]ShareAlike 4.0 International License (https://creativecommons.org/licenses/). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file)[]. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(child) mipTable: Amon
(child) model_doi_url: https://doi.org/10.5065/D67H1H0V
(child) netcdf-scm crunched file: CMIP6/ScenarioMIP/NCAR/CESM2/ssp370/r1i1p1f1/Amon/tas/gn/v20190730/netcdf-scm_tas_Amon_CESM2_ssp370_r1i1p1f1_gn_201501-210012.nc
(child) nominal_resolution: 100 km
(child) out_name: tas
(child) parent_activity_id: CMIP
(child) parent_experiment_id: historical
(child) parent_mip_era: CMIP6
(child) parent_source_id: CESM2
(child) parent_time_units: days since 0001-01-01 00:00:00
(child) parent_variant_label: r10i1p1f1
(child) product: model-output
(child) prov: Amon ((isd.003))
(child) realm: atmos
(child) source: CESM2 (2017): atmosphere: CAM6 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); ocean: POP2 (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m); sea_ice: CICE5.1 (same grid as ocean); land: CLM5 0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); aerosol: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); atmoschem: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); landIce: CISM2.1; ocnBgchem: MARBL (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m)
(child) source_id: CESM2
(child) source_type: AOGCM BGC AER
(child) sub_experiment: none
(child) sub_experiment_id: none
(child) table_id: Amon
(child) time: time
(child) time_label: time-mean
(child) time_title: Temporal mean
(child) title: Near-Surface Air Temperature
(child) tracking_id: hdl:21.14100/a613f9ae-807b-42c5-b417-8a0ab62d39ec
(child) type: real
(child) variable_id: tas
(child) variant_info: CMIP6 SSP3-7.0 experiments (2015-2100) with CAM6, interactive land (CLM5), coupled ocean (POP2) with biogeochemistry (MARBL), interactive sea ice (CICE5.1), and non-evolving land ice (CISM2.1).
(child) variant_label: r1i1p1f1
(normalisation) CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
(normalisation) CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
(normalisation) Conventions: CF-1.7
(normalisation) area_world (m**2): 509194858320979.25
(normalisation) area_world_el_nino_n3.4 (m**2): 9499544514719.764
(normalisation) area_world_land (m**2): 146750573712803.38
(normalisation) area_world_north_atlantic_ocean (m**2): 39884940686492.164
(normalisation) area_world_northern_hemisphere (m**2): 254597429160489.66
(normalisation) area_world_northern_hemisphere_land (m**2): 98924840662008.36
(normalisation) area_world_northern_hemisphere_ocean (m**2): 155672588435179.16
(normalisation) area_world_ocean (m**2): 362444284572689.1
(normalisation) area_world_southern_hemisphere (m**2): 254597429160489.62
(normalisation) area_world_southern_hemisphere_land (m**2): 47825733050795.016
(normalisation) area_world_southern_hemisphere_ocean (m**2): 206771696137510.0
(normalisation) branch_method: standard
(normalisation) branch_time_in_child: 0.0
(normalisation) branch_time_in_parent: 48545.0
(normalisation) calendar: 365_day
(normalisation) case_id: 3
(normalisation) cesm_casename: b.e21.B1850.f09_g17.CMIP6-piControl.001
(normalisation) comment: near-surface (usually, 2 meter) air temperature
(normalisation) contact: cesm_cmip6@ucar.edu
(normalisation) crunch_contact: cmip6output crunching regression test
(normalisation) crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
(normalisation) crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Land", "World|Ocean", "World|Northern Hemisphere|Land", "World|Southern Hemisphere|Land", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
(normalisation) crunch_source_files: Files: ['/CMIP6/CMIP/NCAR/CESM2/piControl/r1i1p1f1/Amon/tas/gn/v20190320/tas_Amon_CESM2_piControl_r1i1p1f1_gn_080001-089912.nc', '/CMIP6/CMIP/NCAR/CESM2/piControl/r1i1p1f1/Amon/tas/gn/v20190320/tas_Amon_CESM2_piControl_r1i1p1f1_gn_090001-099912.nc']
(normalisation) data_specs_version: 01.00.29
(normalisation) description: near-surface (usually, 2 meter) air temperature
(normalisation) experiment: pre-industrial control
(normalisation) experiment_id: piControl
(normalisation) external_variables: areacella
(normalisation) frequency: mon
(normalisation) further_info_url: https://furtherinfo.es-doc.org/CMIP6.NCAR.CESM2.piControl.none.r1i1p1f1
(normalisation) grid: native 0.9x1.25 finite volume grid (192x288 latxlon)
(normalisation) grid_label: gn
(normalisation) id: tas
(normalisation) institution: National Center for Atmospheric Research
(normalisation) institution_id: NCAR
(normalisation) land_fraction: 0.2882012088588231
(normalisation) land_fraction_northern_hemisphere: 0.3885539653255865
(normalisation) land_fraction_southern_hemisphere: 0.18784845239205966
(normalisation) license: CMIP6 model data produced by <The National Center for Atmospheric Research> is licensed under a Creative Commons Attribution-[]ShareAlike 4.0 International License (https://creativecommons.org/licenses/). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file)[]. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(normalisation) mipTable: Amon
(normalisation) model_doi_url: https://doi.org/10.5065/D67H1H0V
(normalisation) netcdf-scm crunched file: CMIP6/CMIP/NCAR/CESM2/piControl/r1i1p1f1/Amon/tas/gn/v20190320/netcdf-scm_tas_Amon_CESM2_piControl_r1i1p1f1_gn_080001-099912.nc
(normalisation) nominal_resolution: 100 km
(normalisation) out_name: tas
(normalisation) parent_activity_id: CMIP
(normalisation) parent_experiment_id: piControl-spinup
(normalisation) parent_mip_era: CMIP6
(normalisation) parent_source_id: CESM2
(normalisation) parent_time_units: days since 0001-01-01 00:00:00
(normalisation) parent_variant_label: r1i1p1f1
(normalisation) product: model-output
(normalisation) prov: Amon ((isd.003))
(normalisation) realm: atmos
(normalisation) source: CESM2 (2017): atmosphere: CAM6 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); ocean: POP2 (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m); sea_ice: CICE5.1 (same grid as ocean); land: CLM5 0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); aerosol: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); atmoschem: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); landIce: CISM2.1; ocnBgchem: MARBL (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m)
(normalisation) source_id: CESM2
(normalisation) source_type: AOGCM BGC AER
(normalisation) sub_experiment: none
(normalisation) sub_experiment_id: none
(normalisation) table_id: Amon
(normalisation) time: time
(normalisation) time_label: time-mean
(normalisation) time_title: Temporal mean
(normalisation) title: Near-Surface Air Temperature
(normalisation) tracking_id: hdl:21.14100/613911cb-8d9b-44a2-9664-2eee8f522acb
(normalisation) type: real
(normalisation) variable_id: tas
(normalisation) variant_info: CMIP6 CESM2 piControl experiment with CAM6, interactive land (CLM5), coupled ocean (POP2) with biogeochemistry (MARBL), interactive sea ice (CICE5.1), and non-evolving land ice (CISM2.1)
(normalisation) variant_label: r1i1p1f1
(parent) CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
(parent) CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
(parent) Conventions: CF-1.7
(parent) area_world (m**2): 509194858320979.25
(parent) area_world_el_nino_n3.4 (m**2): 9499544514719.764
(parent) area_world_land (m**2): 146750573712803.38
(parent) area_world_north_atlantic_ocean (m**2): 39884940686492.164
(parent) area_world_northern_hemisphere (m**2): 254597429160489.66
(parent) area_world_northern_hemisphere_land (m**2): 98924840662008.36
(parent) area_world_northern_hemisphere_ocean (m**2): 155672588435179.16
(parent) area_world_ocean (m**2): 362444284572689.1
(parent) area_world_southern_hemisphere (m**2): 254597429160489.62
(parent) area_world_southern_hemisphere_land (m**2): 47825733050795.016
(parent) area_world_southern_hemisphere_ocean (m**2): 206771696137510.0
(parent) branch_method: standard
(parent) branch_time_in_child: 674885.0
(parent) branch_time_in_parent: 306600.0
(parent) calendar: 365_day
(parent) case_id: 24
(parent) cesm_casename: b.e21.BHIST.f09_g17.CMIP6-historical.010
(parent) comment: near-surface (usually, 2 meter) air temperature
(parent) contact: cesm_cmip6@ucar.edu
(parent) crunch_contact: cmip6output crunching regression test
(parent) crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
(parent) crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Land", "World|Ocean", "World|Northern Hemisphere|Land", "World|Southern Hemisphere|Land", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
(parent) crunch_source_files: Files: ['/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Amon/tas/gn/v20190313/tas_Amon_CESM2_historical_r10i1p1f1_gn_185001-189912.nc', '/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Amon/tas/gn/v20190313/tas_Amon_CESM2_historical_r10i1p1f1_gn_190001-194912.nc', '/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Amon/tas/gn/v20190313/tas_Amon_CESM2_historical_r10i1p1f1_gn_195001-199912.nc', '/CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Amon/tas/gn/v20190313/tas_Amon_CESM2_historical_r10i1p1f1_gn_200001-201412.nc']
(parent) data_specs_version: 01.00.29
(parent) description: near-surface (usually, 2 meter) air temperature
(parent) experiment: Simulation of recent past (1850 to 2014). Impose changing conditions (consistent with observations). Should be initialised from a point early enough in the pre-industrial control run to ensure that the end of all the perturbed runs branching from the end of this historical run end before the end of the control. Only one ensemble member is requested but modelling groups are strongly encouraged to submit at least three ensemble members of their CMIP historical simulation. 
(parent) experiment_id: historical
(parent) external_variables: areacella
(parent) frequency: mon
(parent) further_info_url: https://furtherinfo.es-doc.org/CMIP6.NCAR.CESM2.historical.none.r10i1p1f1
(parent) grid: native 0.9x1.25 finite volume grid (192x288 latxlon)
(parent) grid_label: gn
(parent) id: tas
(parent) institution: National Center for Atmospheric Research
(parent) institution_id: NCAR
(parent) land_fraction: 0.2882012088588231
(parent) land_fraction_northern_hemisphere: 0.3885539653255865
(parent) land_fraction_southern_hemisphere: 0.18784845239205966
(parent) license: CMIP6 model data produced by <The National Center for Atmospheric Research> is licensed under a Creative Commons Attribution-[]ShareAlike 4.0 International License (https://creativecommons.org/licenses/). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file)[]. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(parent) mipTable: Amon
(parent) model_doi_url: https://doi.org/10.5065/D67H1H0V
(parent) netcdf-scm crunched file: CMIP6/CMIP/NCAR/CESM2/historical/r10i1p1f1/Amon/tas/gn/v20190313/netcdf-scm_tas_Amon_CESM2_historical_r10i1p1f1_gn_185001-201412.nc
(parent) nominal_resolution: 100 km
(parent) out_name: tas
(parent) parent_activity_id: CMIP
(parent) parent_experiment_id: piControl
(parent) parent_mip_era: CMIP6
(parent) parent_source_id: CESM2
(parent) parent_time_units: days since 0001-01-01 00:00:00
(parent) parent_variant_label: r1i1p1f1
(parent) product: model-output
(parent) prov: Amon ((isd.003))
(parent) realm: atmos
(parent) source: CESM2 (2017): atmosphere: CAM6 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); ocean: POP2 (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m); sea_ice: CICE5.1 (same grid as ocean); land: CLM5 0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); aerosol: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); atmoschem: MAM4 (0.9x1.25 finite volume grid; 288 x 192 longitude/latitude; 32 levels; top level 2.25 mb); landIce: CISM2.1; ocnBgchem: MARBL (320x384 longitude/latitude; 60 levels; top grid cell 0-10 m)
(parent) source_id: CESM2
(parent) source_type: AOGCM BGC
(parent) sub_experiment: none
(parent) sub_experiment_id: none
(parent) table_id: Amon
(parent) time: time
(parent) time_label: time-mean
(parent) time_title: Temporal mean
(parent) title: Near-Surface Air Temperature
(parent) tracking_id: hdl:21.14100/e47b79db-3925-45a7-9c0a-6799c2f1e8ae
(parent) type: real
(parent) variable_id: tas
(parent) variant_info: CMIP6 20th century experiments (1850-2014) with CAM6, interactive land (CLM5), coupled ocean (POP2) with biogeochemistry (MARBL), interactive sea ice (CICE5.1), and non-evolving land ice (CISM2.1)

(parent) variant_label: r10i1p1f1
normalisation method: 31-yr-mean-after-branch-time
timeseriestype: POINT_START_YEAR

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 1
    THISFILE_DATAROWS = 251
    THISFILE_FIRSTYEAR = 1850
    THISFILE_LASTYEAR = 2100
    THISFILE_ANNUALSTEPS = 1
    THISFILE_UNITS = 'K'
    THISFILE_DATTYPE = 'NOTUSED'
    THISFILE_REGIONMODE = 'FOURBOX'
    THISFILE_FIRSTDATAROW = 244
/

   VARIABLE        SURFACE_TEMP
       TODO                 SET
      UNITS                   K
      YEARS               WORLD
       1850        -2.30228e+00
       1851        -2.06980e+00
       1852        -1.73891e+00
       1853        -1.72522e+00
       1854        -1.84812e+00
       1855        -2.06353e+00
       1856        -1.86352e+00
       1857        -1.84851e+00
       1858        -1.88778e+00
       1859        -1.52262e+00
       1860        -1.78569e+00
       1861        -1.67667e+00
       1862        -1.71645e+00
       1863        -2.21991e+00
       1864        -1.89866e+00
       1865        -1.56407e+00
       1866        -1.64242e+00
       1867        -1.91551e+00
       1868        -1.70755e+00
       1869        -1.56785e+00
       1870        -1.70798e+00
       1871        -1.32502e+00
       1872        -1.74900e+00
       1873        -1.87475e+00
       1874        -1.50630e+00
       1875        -1.67943e+00
       1876        -1.44392e+00
       1877        -1.52988e+00
       1878        -1.83605e+00
       1879        -1.83485e+00
       1880        -1.16745e+00
       1881        -1.61718e+00
       1882        -1.37964e+00
       1883        -1.41180e+00
       1884        -1.62100e+00
       1885        -1.79276e+00
       1886        -1.84595e+00
       1887        -1.81107e+00
       1888        -1.83290e+00
       1889        -1.57825e+00
       1890        -1.63384e+00
       1891        -1.64593e+00
       1892        -1.30113e+00
       1893        -1.30576e+00
       1894        -1.39062e+00
       1895        -1.75627e+00
       1896        -1.61219e+00
       1897        -1.54623e+00
       1898        -1.70214e+00
       1899        -1.72166e+00
       1900        -1.59333e+00
       1901        -1.63733e+00
       1902        -1.64944e+00
       1903        -1.87943e+00
       1904        -1.75182e+00
       1905        -1.60885e+00
       1906        -1.69562e+00
       1907        -1.63462e+00
       1908        -1.66049e+00
       1909        -1.65365e+00
       1910        -1.57340e+00
       1911        -1.68614e+00
       1912        -1.44135e+00
       1913        -1.68149e+00
       1914        -1.51621e+00
       1915        -1.71979e+00
       1916        -1.69808e+00
       1917        -1.76443e+00
       1918        -1.39397e+00
       1919        -1.42607e+00
       1920        -1.29323e+00
       1921        -1.52764e+00
       1922        -1.46365e+00
       1923        -1.54992e+00
       1924        -1.45180e+00
       1925        -1.47229e+00
       1926        -1.39232e+00
       1927        -1.55292e+00
       1928        -1.52708e+00
       1929        -1.39024e+00
       1930        -1.63837e+00
       1931        -1.68407e+00
       1932        -1.39277e+00
       1933        -1.46287e+00
       1934        -1.65209e+00
       1935        -1.44967e+00
       1936        -1.37868e+00
       1937        -1.60001e+00
       1938        -1.56287e+00
       1939        -1.74768e+00
       1940        -1.55412e+00
       1941        -1.34907e+00
       1942        -1.71157e+00
       1943        -1.37019e+00
       1944        -1.45112e+00
       1945        -1.43025e+00
       1946        -1.60845e+00
       1947        -1.64305e+00
       1948        -1.59891e+00
       1949        -1.50910e+00
       1950        -1.34631e+00
       1951        -1.70889e+00
       1952        -1.34912e+00
       1953        -1.35489e+00
       1954        -1.42326e+00
       1955        -1.37668e+00
       1956        -1.30515e+00
       1957        -1.52656e+00
       1958        -1.48636e+00
       1959        -1.55041e+00
       1960        -1.31686e+00
       1961        -1.46141e+00
       1962        -1.38492e+00
       1963        -1.27820e+00
       1964        -1.50436e+00
       1965        -1.62531e+00
       1966        -1.75182e+00
       1967        -1.79919e+00
       1968        -1.87366e+00
       1969        -1.52838e+00
       1970        -1.49363e+00
       1971        -1.20317e+00
       1972        -1.21783e+00
       1973        -1.31105e+00
       1974        -1.58446e+00
       1975        -1.41963e+00
       1976        -1.50367e+00
       1977        -1.41784e+00
       1978        -1.29459e+00
       1979        -1.25760e+00
       1980        -1.62397e+00
       1981        -1.39104e+00
       1982        -1.23529e+00
       1983        -1.31341e+00
       1984        -1.14957e+00
       1985        -1.24990e+00
       1986        -1.27003e+00
       1987        -1.28166e+00
       1988        -1.13587e+00
       1989        -1.13336e+00
       1990        -9.93354e-01
       1991        -1.09212e+00
       1992        -1.59882e+00
       1993        -1.17007e+00
       1994        -7.67399e-01
       1995        -1.10443e+00
       1996        -1.19525e+00
       1997        -8.92445e-01
       1998        -9.51588e-01
       1999        -1.02389e+00
       2000        -1.00796e+00
       2001        -9.58669e-01
       2002        -6.59799e-01
       2003        -5.44459e-01
       2004        -6.04566e-01
       2005        -5.93933e-01
       2006        -6.22601e-01
       2007        -4.96392e-01
       2008        -2.64621e-01
       2009        -5.31605e-01
       2010        -3.98648e-01
       2011        -3.00841e-01
       2012        -6.72027e-01
       2013        -8.45046e-01
       2014        -6.85188e-01
       2015        -2.72096e-01
       2016        -4.97041e-01
       2017        -4.10665e-01
       2018        -5.51428e-01
       2019        -4.54800e-01
       2020        -3.34857e-01
       2021        -2.91465e-01
       2022        -1.67012e-01
       2023        -2.16964e-01
       2024        -9.69800e-02
       2025         1.22834e-01
       2026         4.39809e-02
       2027         1.40989e-01
       2028         7.28993e-02
       2029         1.40331e-01
       2030         2.81958e-01
       2031         4.03633e-02
       2032        -2.58833e-01
       2033         4.19479e-01
       2034         4.60484e-01
       2035         6.78563e-01
       2036         3.39368e-01
       2037         3.69525e-01
       2038         7.19531e-01
       2039         1.86758e-01
       2040         4.24349e-02
       2041         2.55303e-01
       2042         2.70783e-01
       2043         4.23354e-01
       2044         5.30246e-01
       2045         3.81446e-01
       2046         7.63556e-01
       2047         3.50926e-01
       2048         2.57487e-01
       2049         7.86971e-01
       2050         7.95257e-01
       2051         7.65670e-01
       2052         6.51767e-01
       2053         7.23376e-01
       2054         1.02847e+00
       2055         9.90285e-01
       2056         1.09111e+00
       2057         8.82364e-01
       2058         9.84724e-01
       2059         1.36347e+00
       2060         9.94500e-01
       2061         1.20666e+00
       2062         1.46026e+00
       2063         1.33411e+00
       2064         1.33834e+00
       2065         1.43360e+00
       2066         1.73471e+00
       2067         1.35825e+00
       2068         1.53595e+00
       2069         1.60102e+00
       2070         1.72061e+00
       2071         1.58918e+00
       2072         1.62131e+00
       2073         1.81052e+00
       2074         1.88057e+00
       2075         1.77337e+00
       2076         2.20155e+00
       2077         2.04530e+00
       2078         1.91756e+00
       2079         2.45174e+00
       2080         2.05061e+00
       2081         2.18182e+00
       2082         2.63039e+00
       2083         2.28512e+00
       2084         2.50544e+00
       2085         2.65400e+00
       2086         2.77720e+00
       2087         2.61928e+00
       2088         2.94946e+00
       2089         2.76295e+00
       2090         3.10826e+00
       2091         2.83636e+00
       2092         2.96661e+00
       2093         3.08066e+00
       2094         2.79218e+00
       2095         3.24926e+00
       2096         2.88860e+00
       2097         3.05769e+00
       2098         3.39686e+00
       2099         3.16263e+00
       2100         3.50564e+00
