---- HEADER ----

Date: 2021-02-22 07:34:28
Contact: cmip6output wrangling regression test
Source data crunched with: netCDF-SCM v2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
File written with: pymagicc v2.0.0 (more info at github.com/openclimatedata/pymagicc)

For more information on the AR6 regions (including mapping the abbrevations to their full names), see: https://github.com/SantanderMetGroup/ATLAS/tree/master/reference-regions, specifically https://github.com/SantanderMetGroup/ATLAS/blob/master/reference-regions/IPCC-WGI-reference-regions-v4_coordinates.csv (paper is at https://doi.org/10.5194/essd-2019-258)

---- METADATA ----

CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
Conventions: CF-1.7
area_world (m**2): 510045092655104.0
area_world_el_nino_n3.4 (m**2): 6657359020032.0
area_world_north_atlantic_ocean (m**2): 55474658279424.0
area_world_northern_hemisphere (m**2): 225108691435520.0
area_world_northern_hemisphere_ocean (m**2): 225108691435520.0
area_world_ocean (m**2): 510045092655104.0
area_world_southern_hemisphere (m**2): 284936401219584.0
area_world_southern_hemisphere_ocean (m**2): 284936401219584.0
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 65744.0
calendar: gregorian
cell_measures: area: areacello
cmor_version: 3.4.0
comment: This is the net flux of heat entering the liquid water column through its upper surface (excluding any 'flux adjustment') .
contact: cmip6-data@ec-earth.org
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Ocean", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
crunch_source_files: Files: ['/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-Veg/historical/r1i1p1f1/Omon/hfds/gn/v20190605/hfds_Omon_EC-Earth3-Veg_historical_r1i1p1f1_gn_201201-201212.nc', '/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-Veg/historical/r1i1p1f1/Omon/hfds/gn/v20190605/hfds_Omon_EC-Earth3-Veg_historical_r1i1p1f1_gn_201301-201312.nc', '/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-Veg/historical/r1i1p1f1/Omon/hfds/gn/v20190605/hfds_Omon_EC-Earth3-Veg_historical_r1i1p1f1_gn_201401-201412.nc']; areacello: ['/CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-Veg/historical/r1i1p1f1/Ofx/areacello/gn/v20190605/areacello_Ofx_EC-Earth3-Veg_historical_r1i1p1f1_gn.nc']
data_specs_version: 01.00.30
experiment: all-forcing simulation of the recent past
experiment_id: historical
external_variables: areacello
forcing_index: 1
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.EC-Earth-Consortium.EC-Earth3-Veg.historical.none.r1i1p1f1
grid: T255L91-ORCA1L75
grid_label: gn
initialization_index: 1
institution: AEMET, Spain; BSC, Spain; CNR-ISAC, Italy; DMI, Denmark; ENEA, Italy; FMI, Finland; Geomar, Germany; ICHEC, Ireland; ICTP, Italy; IDL, Portugal; IMAU, The Netherlands; IPMA, Portugal; KIT, Karlsruhe, Germany; KNMI, The Netherlands; Lund University, Sweden; Met Eireann, Ireland; NLeSC, The Netherlands; NTNU, Norway; Oxford University, UK; surfSARA, The Netherlands; SMHI, Sweden; Stockholm University, Sweden; Unite ASTR, Belgium; University College Dublin, Ireland; University of Bergen, Norway; University of Copenhagen, Denmark; University of Helsinki, Finland; University of Santiago de Compostela, Spain; Uppsala University, Sweden; Utrecht University, The Netherlands; Vrije Universiteit Amsterdam, the Netherlands; Wageningen University, The Netherlands. Mailing address: EC-Earth consortium, Rossby Center, Swedish Meteorological and Hydrological Institute/SMHI, SE-601 76 Norrkoping, Sweden
institution_id: EC-Earth-Consortium
license: CMIP6 model data produced by EC-Earth-Consortium is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at http://www.ec-earth.org. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
netcdf-scm crunched file: CMIP6/CMIP/EC-Earth-Consortium/EC-Earth3-Veg/historical/r1i1p1f1/Omon/hfds/gn/v20190605/netcdf-scm_hfds_Omon_EC-Earth3-Veg_historical_r1i1p1f1_gn_201201-201412.nc
nominal_resolution: 100 km
original_name: hfds
parent_activity_id: CMIP
parent_experiment_id: piControl
parent_mip_era: CMIP6
parent_source_id: EC-Earth3-Veg
parent_time_units: days since 1850-01-01
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: ocean
source: EC-Earth3-Veg (2019): 
aerosol: none
atmos: IFS cy36r4 (TL255, linearly reduced Gaussian grid equivalent to 512 x 256 longitude/latitude; 91 levels; top level 0.01 hPa)
atmosChem: none
land: HTESSEL (land surface scheme built in IFS) and LPJ-GUESS v4
landIce: none
ocean: NEMO3.6 (ORCA1 tripolar primarily 1 degree with meridional refinement down to 1/3 degree in the tropics; 362 x 292 longitude/latitude; 75 levels; top grid cell 0-1 m)
ocnBgchem: none
seaIce: LIM3
source_id: EC-Earth3-Veg
source_type: AOGCM
sub_experiment: none
sub_experiment_id: none
table_id: Omon
table_info: Creation Date:(09 May 2019) MD5:cde930676e68ac6780d5e4c62d3898f6
title: EC-Earth3-Veg output prepared for CMIP6
variable_id: hfds
variant_label: r1i1p1f1

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 8
    THISFILE_DATAROWS = 36
    THISFILE_FIRSTYEAR = 2012
    THISFILE_LASTYEAR = 2014
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 96
/

   VARIABLE                hfds                hfds                hfds                hfds                hfds                hfds                hfds                hfds
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2
      YEARS               WORLD                  NH                  SH               OCEAN             NHOCEAN             SHOCEAN                 AMV                 N34
2012.042000         6.09994e+00        -1.04360e+02         9.33668e+01         6.09994e+00        -1.04360e+02         9.33668e+01        -1.31940e+02         4.20667e+01
2012.125000         2.72503e+00        -7.17850e+01         6.15903e+01         2.72503e+00        -7.17850e+01         6.15903e+01        -7.25542e+01         6.05139e+01
2012.208000        -2.48865e-01        -1.69342e+01         1.29331e+01        -2.48865e-01        -1.69342e+01         1.29331e+01        -2.67897e+01         6.42782e+01
2012.292000        -9.44095e+00         2.48520e+01        -3.65335e+01        -9.44095e+00         2.48520e+01        -3.65335e+01         5.30091e+00         7.12687e+01
2012.375000        -1.87506e+01         6.41233e+01        -8.42235e+01        -1.87506e+01         6.41233e+01        -8.42235e+01         7.04493e+01         4.75103e+01
2012.458000        -2.28179e+01         7.97790e+01        -1.03873e+02        -2.28179e+01         7.97790e+01        -1.03873e+02         9.09213e+01         4.15238e+01
2012.542000        -2.27796e+01         7.33478e+01        -9.87233e+01        -2.27796e+01         7.33478e+01        -9.87233e+01         8.54514e+01         3.14823e+01
2012.625000        -1.22644e+01         5.35719e+01        -6.42772e+01        -1.22644e+01         5.35719e+01        -6.42772e+01         6.21627e+01         5.87500e+01
2012.708000         1.86888e+00         1.57455e+01        -9.09406e+00         1.86888e+00         1.57455e+01        -9.09406e+00         1.36766e+01         6.83404e+01
2012.792000         1.24186e+01        -3.50989e+01         4.99590e+01         1.24186e+01        -3.50989e+01         4.99590e+01        -3.41867e+01         5.92635e+01
2012.875000         1.19608e+01        -8.64984e+01         8.97466e+01         1.19608e+01        -8.64984e+01         8.97466e+01        -1.24566e+02         5.81606e+01
2012.958000         7.27828e+00        -1.14041e+02         1.03125e+02         7.27828e+00        -1.14041e+02         1.03125e+02        -1.40810e+02         5.10606e+01
2013.042000         6.80849e+00        -1.01460e+02         9.23441e+01         6.80849e+00        -1.01460e+02         9.23441e+01        -1.31908e+02         8.26508e+01
2013.125000         2.74800e+00        -6.79450e+01         5.85977e+01         2.74800e+00        -6.79450e+01         5.85977e+01        -1.10530e+02         8.36906e+01
2013.208000        -8.09006e-02        -1.63791e+01         1.27952e+01        -8.09006e-02        -1.63791e+01         1.27952e+01        -3.47373e+01         9.65187e+01
2013.292000        -7.35637e+00         2.95700e+01        -3.65294e+01        -7.35637e+00         2.95700e+01        -3.65294e+01         1.28573e+01         9.08374e+01
2013.375000        -1.83490e+01         6.31213e+01        -8.27131e+01        -1.83490e+01         6.31213e+01        -8.27131e+01         6.21426e+01         6.74586e+01
2013.458000        -2.43050e+01         7.97353e+01        -1.06500e+02        -2.43050e+01         7.97353e+01        -1.06500e+02         9.30156e+01         3.10293e+01
2013.542000        -2.17348e+01         6.90994e+01        -9.34966e+01        -2.17348e+01         6.90994e+01        -9.34966e+01         8.99808e+01         2.89935e+01
2013.625000        -8.80052e+00         5.68262e+01        -6.06477e+01        -8.80052e+00         5.68262e+01        -6.06477e+01         6.72249e+01         4.82152e+01
2013.708000         3.13657e+00         1.65053e+01        -7.42516e+00         3.13657e+00         1.65053e+01        -7.42516e+00         1.58114e+01         5.17494e+01
2013.792000         1.00305e+01        -3.48397e+01         4.54793e+01         1.00305e+01        -3.48397e+01         4.54793e+01        -4.60617e+01         6.27014e+01
2013.875000         1.24935e+01        -8.47652e+01         8.93309e+01         1.24935e+01        -8.47652e+01         8.93309e+01        -1.01275e+02         5.25808e+01
2013.958000         6.40172e+00        -1.18330e+02         1.04943e+02         6.40172e+00        -1.18330e+02         1.04943e+02        -1.39697e+02         4.99805e+01
2014.042000         7.20451e+00        -1.02695e+02         9.40287e+01         7.20451e+00        -1.02695e+02         9.40287e+01        -1.44892e+02         8.21138e+01
2014.125000         6.77561e+00        -6.23369e+01         6.13767e+01         6.77561e+00        -6.23369e+01         6.13767e+01        -8.81491e+01         8.30763e+01
2014.208000         1.77162e-01        -2.16316e+01         1.74068e+01         1.77162e-01        -2.16316e+01         1.74068e+01        -2.66442e+01         8.80797e+01
2014.292000        -7.58483e+00         3.22615e+01        -3.90647e+01        -7.58483e+00         3.22615e+01        -3.90647e+01         2.77997e+01         7.64994e+01
2014.375000        -1.81703e+01         6.22116e+01        -8.16745e+01        -1.81703e+01         6.22116e+01        -8.16745e+01         5.90640e+01         5.39892e+01
2014.458000        -2.57922e+01         7.62069e+01        -1.06375e+02        -2.57922e+01         7.62069e+01        -1.06375e+02         9.26365e+01         2.37621e+01
2014.542000        -2.26372e+01         7.30773e+01        -9.82546e+01        -2.26372e+01         7.30773e+01        -9.82546e+01         8.68546e+01         2.09597e+01
2014.625000        -1.25167e+01         5.38414e+01        -6.49416e+01        -1.25167e+01         5.38414e+01        -6.49416e+01         6.05079e+01         2.39560e+01
2014.708000         2.37691e+00         1.96006e+01        -1.12303e+01         2.37691e+00         1.96006e+01        -1.12303e+01         1.57502e+01         5.86220e+01
2014.792000         1.07778e+01        -3.57985e+01         4.75746e+01         1.07778e+01        -3.57985e+01         4.75746e+01        -4.19244e+01         5.59226e+01
2014.875000         9.53376e+00        -9.03456e+01         8.84416e+01         9.53376e+00        -9.03456e+01         8.84416e+01        -1.04016e+02         5.12396e+01
2014.958000         9.39312e+00        -1.08116e+02         1.02229e+02         9.39312e+00        -1.08116e+02         1.02229e+02        -1.58144e+02         3.08790e+01
