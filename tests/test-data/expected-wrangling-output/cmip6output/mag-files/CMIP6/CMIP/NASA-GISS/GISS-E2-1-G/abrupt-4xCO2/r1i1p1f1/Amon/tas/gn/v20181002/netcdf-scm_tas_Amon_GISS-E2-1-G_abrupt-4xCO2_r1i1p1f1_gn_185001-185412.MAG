---- HEADER ----

Date: 2021-02-22 07:34:27
Contact: cmip6output wrangling regression test
Source data crunched with: netCDF-SCM v2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
File written with: pymagicc v2.0.0 (more info at github.com/openclimatedata/pymagicc)

For more information on the AR6 regions (including mapping the abbrevations to their full names), see: https://github.com/SantanderMetGroup/ATLAS/tree/master/reference-regions, specifically https://github.com/SantanderMetGroup/ATLAS/blob/master/reference-regions/IPCC-WGI-reference-regions-v4_coordinates.csv (paper is at https://doi.org/10.5194/essd-2019-258)

---- METADATA ----

CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
Conventions: CF-1.7
area_world (m**2): 509499402446956.25
area_world_el_nino_n3.4 (m**2): 7396831885217.781
area_world_land (m**2): 146362392041847.12
area_world_north_atlantic_ocean (m**2): 39715083631328.734
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98713240066379.7
area_world_northern_hemisphere_ocean (m**2): 156036461182380.62
area_world_ocean (m**2): 363137010439362.75
area_world_southern_hemisphere (m**2): 254749701223478.1
area_world_southern_hemisphere_land (m**2): 47649151975467.445
area_world_southern_hemisphere_ocean (m**2): 207100549256982.1
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 0.0
calendar: 365_day
cmor_version: 3.3.2
comment: near-surface (usually, 2 meter) air temperature
contact: Kenneth Lo (cdkkl@giss.nasa.gov)
creation_date: 2018-10-03T00:27:54Z
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Land", "World|Ocean", "World|Northern Hemisphere|Land", "World|Southern Hemisphere|Land", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
crunch_source_files: Files: ['/CMIP6/CMIP/NASA-GISS/GISS-E2-1-G/abrupt-4xCO2/r1i1p1f1/Amon/tas/gn/v20181002/tas_Amon_GISS-E2-1-G_abrupt-4xCO2_r1i1p1f1_gn_185001-185412.nc']
data_specs_version: 01.00.23
experiment: abrupt quadrupling of CO2
experiment_id: abrupt-4xCO2
external_variables: areacella
forcing_index: 1
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.NASA-GISS.GISS-E2-1-G.abrupt-4xCO2.none.r1i1p1f1
grid: atmospheric grid: 144x90, ocean grid: 288x180
grid_label: gn
history: 2018-10-03T00:27:54Z altered by CMOR: Treated scalar dimension: 'height'. 2018-10-03T00:27:54Z altered by CMOR: replaced missing value flag (-1e+30) with standard missing value (1e+20).
initialization_index: 1
institution: Goddard Institute for Space Studies, New York, NY 10025, USA
institution_id: NASA-GISS
land_fraction: 0.28726705338400244
land_fraction_northern_hemisphere: 0.38749109259909953
land_fraction_southern_hemisphere: 0.18704301416890545
license: CMIP6 model data produced by NASA Goddard Institute for Space Studies is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https:///pcmdi.llnl.gov/. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
model_id: E200_4xCO2aF40oQ40_2.json
nominal_resolution: 250 km
parent_activity_id: CMIP
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
parent_mip_era: CMIP6
parent_source_id: GISS-E2-1-G
parent_time_units: days since 4150-1-1
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: atmos
references: https://data.giss.nasa.gov/modelE/cmip6
source: GISS-E2.1G (2016): 
aerosol: Varies with physics-version (p==1 none, p==3 OMA, p==4 TOMAS, p==5 MATRIX)
atmos: GISS-E2.1 (2.5x2 degree; 144 x 90 longitude/latitude; 40 levels; top level 0.1 hPa)
atmosChem: Varies with physics-version (p==1 Non-interactive, p>1 GPUCCINI)
land: GISS LSM
landIce: none
ocean: GISS Ocean (1.25x1 degree; 288 x 180 longitude/latitude; 32 levels; top grid cell 0-10 m)
ocnBgchem: none
seaIce: GISS SI
source_id: GISS-E2-1-G
source_type: AOGCM
sub_experiment: none
sub_experiment_id: none
table_id: Amon
table_info: Creation Date:(21 March 2018) MD5:f76dbc1e8bf6b7e4aee30573a09e5454
title: GISS-E2-1-G output prepared for CMIP6
tracking_id: hdl:21.14100/5a56a8c4-c09a-4e5c-ae53-fd66055789fa
variable_id: tas
variant_label: r1i1p1f1

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 60
    THISFILE_FIRSTYEAR = 1850
    THISFILE_LASTYEAR = 1854
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 105
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                  NH                  SH                LAND               OCEAN              NHLAND              SHLAND             NHOCEAN             SHOCEAN                 AMV                 N34
1850.042000         2.85643e+02         2.81162e+02         2.90123e+02         2.76918e+02         2.89159e+02         2.72571e+02         2.85923e+02         2.86596e+02         2.91090e+02         2.89079e+02         2.99958e+02
1850.125000         2.86140e+02         2.81855e+02         2.90425e+02         2.77787e+02         2.89506e+02         2.74482e+02         2.84635e+02         2.86519e+02         2.91757e+02         2.89045e+02         3.00294e+02
1850.208000         2.86849e+02         2.84014e+02         2.89685e+02         2.79659e+02         2.89747e+02         2.78862e+02         2.81310e+02         2.87273e+02         2.91612e+02         2.89277e+02         3.00616e+02
1850.292000         2.87727e+02         2.86918e+02         2.88536e+02         2.82171e+02         2.89966e+02         2.83699e+02         2.79007e+02         2.88954e+02         2.90728e+02         2.90102e+02         3.00735e+02
1850.375000         2.88747e+02         2.90322e+02         2.87171e+02         2.85022e+02         2.90248e+02         2.88838e+02         2.77115e+02         2.91261e+02         2.89485e+02         2.91747e+02         3.01106e+02
1850.458000         2.89525e+02         2.93229e+02         2.85822e+02         2.87331e+02         2.90409e+02         2.93253e+02         2.75064e+02         2.93213e+02         2.88297e+02         2.93587e+02         3.01447e+02
1850.542000         2.90073e+02         2.95074e+02         2.85071e+02         2.89037e+02         2.90490e+02         2.95912e+02         2.74796e+02         2.94545e+02         2.87436e+02         2.95085e+02         3.01086e+02
1850.625000         2.89868e+02         2.95057e+02         2.84679e+02         2.88506e+02         2.90417e+02         2.95011e+02         2.75030e+02         2.95087e+02         2.86899e+02         2.95734e+02         3.00741e+02
1850.708000         2.89180e+02         2.93255e+02         2.85106e+02         2.86547e+02         2.90242e+02         2.91315e+02         2.76670e+02         2.94483e+02         2.87046e+02         2.95675e+02         3.00803e+02
1850.792000         2.88228e+02         2.89960e+02         2.86497e+02         2.83664e+02         2.90068e+02         2.85432e+02         2.80003e+02         2.92824e+02         2.87992e+02         2.94356e+02         3.01403e+02
1850.875000         2.87612e+02         2.86859e+02         2.88366e+02         2.81658e+02         2.90012e+02         2.80495e+02         2.84067e+02         2.90885e+02         2.89355e+02         2.92728e+02         3.01850e+02
1850.958000         2.86820e+02         2.83632e+02         2.90009e+02         2.79264e+02         2.89866e+02         2.75718e+02         2.86611e+02         2.88638e+02         2.90791e+02         2.91568e+02         3.02060e+02
1851.042000         2.86617e+02         2.82170e+02         2.91065e+02         2.77821e+02         2.90163e+02         2.73393e+02         2.86996e+02         2.87722e+02         2.92002e+02         2.90479e+02         3.01999e+02
1851.125000         2.87332e+02         2.83161e+02         2.91504e+02         2.79844e+02         2.90351e+02         2.76493e+02         2.86786e+02         2.87379e+02         2.92590e+02         2.90135e+02         3.02081e+02
1851.208000         2.87861e+02         2.85023e+02         2.90699e+02         2.81084e+02         2.90592e+02         2.80120e+02         2.83080e+02         2.88125e+02         2.92452e+02         2.90197e+02         3.02199e+02
1851.292000         2.88913e+02         2.88265e+02         2.89560e+02         2.83933e+02         2.90920e+02         2.85594e+02         2.80494e+02         2.89955e+02         2.91646e+02         2.91079e+02         3.02264e+02
1851.375000         2.89863e+02         2.91568e+02         2.88158e+02         2.86442e+02         2.91242e+02         2.90410e+02         2.78222e+02         2.92301e+02         2.90445e+02         2.92531e+02         3.02328e+02
1851.458000         2.90666e+02         2.94519e+02         2.86812e+02         2.89185e+02         2.91262e+02         2.95131e+02         2.76867e+02         2.94132e+02         2.89100e+02         2.94164e+02         3.02514e+02
1851.542000         2.90860e+02         2.96000e+02         2.85719e+02         2.90052e+02         2.91185e+02         2.97099e+02         2.75452e+02         2.95305e+02         2.88081e+02         2.95570e+02         3.01344e+02
1851.625000         2.90674e+02         2.95893e+02         2.85455e+02         2.89633e+02         2.91094e+02         2.96084e+02         2.76270e+02         2.95773e+02         2.87569e+02         2.96298e+02         3.00617e+02
1851.708000         2.90033e+02         2.94052e+02         2.86014e+02         2.87687e+02         2.90979e+02         2.92148e+02         2.78445e+02         2.95257e+02         2.87755e+02         2.96255e+02         3.00829e+02
1851.792000         2.89128e+02         2.91011e+02         2.87245e+02         2.85130e+02         2.90739e+02         2.86722e+02         2.81831e+02         2.93724e+02         2.88491e+02         2.94983e+02         3.01310e+02
1851.875000         2.87931e+02         2.87148e+02         2.88714e+02         2.81538e+02         2.90508e+02         2.80070e+02         2.84580e+02         2.91626e+02         2.89665e+02         2.93500e+02         3.01956e+02
1851.958000         2.87491e+02         2.84512e+02         2.90470e+02         2.79912e+02         2.90546e+02         2.76162e+02         2.87681e+02         2.89794e+02         2.91112e+02         2.92099e+02         3.02349e+02
1852.042000         2.87340e+02         2.83248e+02         2.91433e+02         2.79384e+02         2.90547e+02         2.75294e+02         2.87856e+02         2.88279e+02         2.92256e+02         2.91241e+02         3.02468e+02
1852.125000         2.87478e+02         2.83326e+02         2.91631e+02         2.79299e+02         2.90775e+02         2.76098e+02         2.85929e+02         2.87898e+02         2.92943e+02         2.90807e+02         3.02466e+02
1852.208000         2.88159e+02         2.85352e+02         2.90967e+02         2.81255e+02         2.90942e+02         2.80232e+02         2.83374e+02         2.88591e+02         2.92713e+02         2.90902e+02         3.02535e+02
1852.292000         2.89317e+02         2.88841e+02         2.89792e+02         2.84292e+02         2.91342e+02         2.86018e+02         2.80716e+02         2.90627e+02         2.91880e+02         2.91798e+02         3.02595e+02
1852.375000         2.90195e+02         2.92034e+02         2.88356e+02         2.86924e+02         2.91513e+02         2.90988e+02         2.78504e+02         2.92695e+02         2.90623e+02         2.93239e+02         3.02755e+02
1852.458000         2.90913e+02         2.94860e+02         2.86966e+02         2.89376e+02         2.91532e+02         2.95517e+02         2.76654e+02         2.94444e+02         2.89338e+02         2.94864e+02         3.02851e+02
1852.542000         2.91159e+02         2.96409e+02         2.85910e+02         2.90413e+02         2.91460e+02         2.97644e+02         2.75431e+02         2.95627e+02         2.88321e+02         2.96246e+02         3.02315e+02
1852.625000         2.91086e+02         2.96381e+02         2.85791e+02         2.90172e+02         2.91455e+02         2.96662e+02         2.76726e+02         2.96203e+02         2.87877e+02         2.96979e+02         3.01069e+02
1852.708000         2.90450e+02         2.94587e+02         2.86313e+02         2.88494e+02         2.91238e+02         2.92936e+02         2.79292e+02         2.95632e+02         2.87928e+02         2.96536e+02         3.01008e+02
1852.792000         2.89398e+02         2.91551e+02         2.87245e+02         2.85384e+02         2.91016e+02         2.87440e+02         2.81125e+02         2.94152e+02         2.88653e+02         2.95372e+02         3.01803e+02
1852.875000         2.88462e+02         2.87929e+02         2.88995e+02         2.82394e+02         2.90908e+02         2.81181e+02         2.84907e+02         2.92197e+02         2.89936e+02         2.93948e+02         3.02237e+02
1852.958000         2.88027e+02         2.85268e+02         2.90786e+02         2.81094e+02         2.90822e+02         2.77578e+02         2.88377e+02         2.90133e+02         2.91341e+02         2.92340e+02         3.02613e+02
1853.042000         2.87985e+02         2.84190e+02         2.91780e+02         2.80360e+02         2.91058e+02         2.76494e+02         2.88369e+02         2.89058e+02         2.92565e+02         2.91223e+02         3.02646e+02
1853.125000         2.88026e+02         2.84160e+02         2.91892e+02         2.80369e+02         2.91112e+02         2.77257e+02         2.86815e+02         2.88527e+02         2.93060e+02         2.90737e+02         3.02542e+02
1853.208000         2.88578e+02         2.85963e+02         2.91194e+02         2.82057e+02         2.91207e+02         2.81200e+02         2.83832e+02         2.88976e+02         2.92888e+02         2.90891e+02         3.02494e+02
1853.292000         2.89584e+02         2.89013e+02         2.90154e+02         2.84822e+02         2.91503e+02         2.86242e+02         2.81879e+02         2.90766e+02         2.92058e+02         2.91688e+02         3.01814e+02
1853.375000         2.90378e+02         2.92171e+02         2.88584e+02         2.87223e+02         2.91649e+02         2.91170e+02         2.79046e+02         2.92805e+02         2.90779e+02         2.93011e+02         3.01426e+02
1853.458000         2.91101e+02         2.95030e+02         2.87171e+02         2.89931e+02         2.91572e+02         2.95901e+02         2.77564e+02         2.94480e+02         2.89381e+02         2.94583e+02         3.00696e+02
1853.542000         2.91388e+02         2.96536e+02         2.86240e+02         2.91040e+02         2.91528e+02         2.98028e+02         2.76563e+02         2.95592e+02         2.88466e+02         2.96059e+02         2.99427e+02
1853.625000         2.91016e+02         2.96331e+02         2.85700e+02         2.90155e+02         2.91362e+02         2.96770e+02         2.76450e+02         2.96053e+02         2.87828e+02         2.96924e+02         2.98523e+02
1853.708000         2.90390e+02         2.94565e+02         2.86216e+02         2.88368e+02         2.91205e+02         2.93000e+02         2.78772e+02         2.95554e+02         2.87928e+02         2.96513e+02         2.98346e+02
1853.792000         2.89396e+02         2.91615e+02         2.87178e+02         2.85520e+02         2.90959e+02         2.87728e+02         2.80945e+02         2.94073e+02         2.88612e+02         2.95429e+02         2.99011e+02
1853.875000         2.88510e+02         2.88163e+02         2.88856e+02         2.82768e+02         2.90824e+02         2.82010e+02         2.84340e+02         2.92056e+02         2.89895e+02         2.93877e+02         2.99824e+02
1853.958000         2.87825e+02         2.85263e+02         2.90387e+02         2.80447e+02         2.90799e+02         2.77324e+02         2.86918e+02         2.90285e+02         2.91186e+02         2.92220e+02         2.99969e+02
1854.042000         2.87723e+02         2.84088e+02         2.91359e+02         2.79761e+02         2.90933e+02         2.76294e+02         2.86943e+02         2.89018e+02         2.92375e+02         2.91132e+02         2.99929e+02
1854.125000         2.87841e+02         2.84110e+02         2.91572e+02         2.79966e+02         2.91015e+02         2.77294e+02         2.85504e+02         2.88423e+02         2.92968e+02         2.90922e+02         3.00041e+02
1854.208000         2.88666e+02         2.86106e+02         2.91226e+02         2.82171e+02         2.91284e+02         2.81276e+02         2.84024e+02         2.89162e+02         2.92883e+02         2.91046e+02         3.00799e+02
1854.292000         2.89647e+02         2.89141e+02         2.90153e+02         2.84990e+02         2.91524e+02         2.86513e+02         2.81834e+02         2.90803e+02         2.92067e+02         2.91508e+02         3.01291e+02
1854.375000         2.90290e+02         2.92125e+02         2.88455e+02         2.86875e+02         2.91666e+02         2.91010e+02         2.78310e+02         2.92830e+02         2.90789e+02         2.93066e+02         3.01300e+02
1854.458000         2.90944e+02         2.94833e+02         2.87055e+02         2.89321e+02         2.91599e+02         2.95426e+02         2.76671e+02         2.94458e+02         2.89444e+02         2.94603e+02         3.00720e+02
1854.542000         2.91391e+02         2.96467e+02         2.86314e+02         2.91009e+02         2.91545e+02         2.97777e+02         2.76986e+02         2.95638e+02         2.88460e+02         2.95982e+02         2.99885e+02
1854.625000         2.91159e+02         2.96491e+02         2.85827e+02         2.90419e+02         2.91457e+02         2.96862e+02         2.77070e+02         2.96256e+02         2.87842e+02         2.96896e+02         2.99091e+02
1854.708000         2.90488e+02         2.94641e+02         2.86335e+02         2.88251e+02         2.91390e+02         2.92834e+02         2.78756e+02         2.95785e+02         2.88079e+02         2.96545e+02         2.98946e+02
1854.792000         2.89403e+02         2.91589e+02         2.87217e+02         2.85251e+02         2.91076e+02         2.87407e+02         2.80784e+02         2.94234e+02         2.88697e+02         2.95564e+02         2.98944e+02
1854.875000         2.88531e+02         2.88195e+02         2.88867e+02         2.82533e+02         2.90949e+02         2.81709e+02         2.84239e+02         2.92299e+02         2.89932e+02         2.94132e+02         2.99681e+02
1854.958000         2.87969e+02         2.85394e+02         2.90544e+02         2.80574e+02         2.90949e+02         2.77395e+02         2.87159e+02         2.90454e+02         2.91322e+02         2.92485e+02         3.00226e+02
