---- HEADER ----

Date: 2021-02-22 07:34:30
Contact: cmip6output wrangling regression test
Source data crunched with: netCDF-SCM v2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
File written with: pymagicc v2.0.0 (more info at github.com/openclimatedata/pymagicc)

For more information on the AR6 regions (including mapping the abbrevations to their full names), see: https://github.com/SantanderMetGroup/ATLAS/tree/master/reference-regions, specifically https://github.com/SantanderMetGroup/ATLAS/blob/master/reference-regions/IPCC-WGI-reference-regions-v4_coordinates.csv (paper is at https://doi.org/10.5194/essd-2019-258)

---- METADATA ----

CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
Conventions: CF-1.7
area_world (m**2): 509499402446956.25
area_world_el_nino_n3.4 (m**2): 6801325787112.107
area_world_land (m**2): 146304220254921.3
area_world_north_atlantic_ocean (m**2): 39223844793267.68
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98684474413169.94
area_world_northern_hemisphere_ocean (m**2): 156065226805407.5
area_world_ocean (m**2): 363195182172584.1
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47619745841751.38
area_world_southern_hemisphere_ocean (m**2): 207129955367176.62
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 365242.0
calendar: gregorian
cmor_version: 3.3.2
comment: at the top of the atmosphere (to be compared with satellite measurements)
creation_date: 2018-11-30T08:51:05Z
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 2.0.0+5.g777ad4e8.dirty (see Nicholls et al. (2021, https://doi.org/10.1002/gdj3.113), more info at gitlab.com/netcdf-scm/netcdf-scm)
crunch_netcdf_scm_weight_kwargs: {"regions": ["World", "World|Northern Hemisphere", "World|Southern Hemisphere", "World|Land", "World|Ocean", "World|Northern Hemisphere|Land", "World|Southern Hemisphere|Land", "World|Northern Hemisphere|Ocean", "World|Southern Hemisphere|Ocean", "World|North Atlantic Ocean", "World|El Nino N3.4"], "cell_weights": null}
crunch_source_files: Files: ['/CMIP6/CMIP/MIROC/MIROC6/piControl/r1i1p1f1/Amon/rlut/gn/v20181212/rlut_Amon_MIROC6_piControl_r1i1p1f1_gn_320001-320212.nc']
data_specs_version: 01.00.28
experiment: pre-industrial control
experiment_id: piControl
external_variables: areacella
forcing_index: 1
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.MIROC.MIROC6.piControl.none.r1i1p1f1
grid: native atmosphere T85 Gaussian grid
grid_label: gn
history: 2018-11-30T08:51:05Z altered by CMOR: Converted units from 'W/m**2' to 'W m-2'. 2018-11-30T08:51:05Z altered by CMOR: replaced missing value flag (-999) with standard missing value (1e+20). 2018-11-30T08:51:05Z altered by CMOR: Inverted axis: lat.
initialization_index: 1
institution: JAMSTEC (Japan Agency for Marine-Earth Science and Technology, Kanagawa 236-0001, Japan), AORI (Atmosphere and Ocean Research Institute, The University of Tokyo, Chiba 277-8564, Japan), NIES (National Institute for Environmental Studies, Ibaraki 305-8506, Japan), and R-CCS (RIKEN Center for Computational Science, Hyogo 650-0047, Japan)
institution_id: MIROC
land_fraction: 0.2871528789872388
land_fraction_northern_hemisphere: 0.3873781752803682
land_fraction_southern_hemisphere: 0.18692758269410945
license: CMIP6 model data produced by MIROC is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses/). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file). The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
nominal_resolution: 250 km
original_name: OLR
original_units: W/m**2
parent_activity_id: CMIP
parent_experiment_id: piControl-spinup
parent_mip_era: CMIP6
parent_source_id: MIROC6
parent_time_units: days since 2200-1-1
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: atmos
source: MIROC6 (2017): 
aerosol: SPRINTARS6.0
atmos: CCSR AGCM (T85; 256 x 128 longitude/latitude; 81 levels; top level 0.004 hPa)
atmosChem: none
land: MATSIRO6.0
landIce: none
ocean: COCO4.9 (tripolar primarily 1deg; 360 x 256 longitude/latitude; 63 levels; top grid cell 0-2 m)
ocnBgchem: none
seaIce: COCO4.9
source_id: MIROC6
source_type: AOGCM AER
sub_experiment: none
sub_experiment_id: none
table_id: Amon
table_info: Creation Date:(06 November 2018) MD5:0728c79344e0f262bb76e4f9ff0d9afc
title: MIROC6 output prepared for CMIP6
tracking_id: hdl:21.14100/5887d013-c674-415b-bfc0-c2b9856d06de
variable_id: rlut
variant_label: r1i1p1f1

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 36
    THISFILE_FIRSTYEAR = 3200
    THISFILE_LASTYEAR = 3202
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 103
/

   VARIABLE                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2          W_msuper-2
      YEARS               WORLD                  NH                  SH                LAND               OCEAN              NHLAND              SHLAND             NHOCEAN             SHOCEAN                 AMV                 N34
3200.042000         2.28355e+02         2.24076e+02         2.32634e+02         2.14863e+02         2.33790e+02         2.10838e+02         2.23206e+02         2.32447e+02         2.34802e+02         2.36550e+02         2.68935e+02
3200.125000         2.27867e+02         2.24147e+02         2.31586e+02         2.14521e+02         2.33243e+02         2.13306e+02         2.17039e+02         2.31002e+02         2.34931e+02         2.33648e+02         2.67291e+02
3200.208000         2.27871e+02         2.26364e+02         2.29378e+02         2.15947e+02         2.32675e+02         2.16403e+02         2.15002e+02         2.32663e+02         2.32684e+02         2.35891e+02         2.63124e+02
3200.292000         2.27707e+02         2.27237e+02         2.28178e+02         2.21668e+02         2.30140e+02         2.23523e+02         2.17823e+02         2.29585e+02         2.30558e+02         2.35324e+02         2.63200e+02
3200.375000         2.30334e+02         2.30877e+02         2.29792e+02         2.30309e+02         2.30345e+02         2.34672e+02         2.21267e+02         2.28477e+02         2.31752e+02         2.31990e+02         2.67839e+02
3200.458000         2.32958e+02         2.36783e+02         2.29133e+02         2.39781e+02         2.30209e+02         2.46838e+02         2.25158e+02         2.30425e+02         2.30047e+02         2.31507e+02         2.61656e+02
3200.542000         2.34611e+02         2.38630e+02         2.30592e+02         2.40249e+02         2.32340e+02         2.46622e+02         2.27042e+02         2.33576e+02         2.31408e+02         2.42977e+02         2.64603e+02
3200.625000         2.33900e+02         2.36980e+02         2.30819e+02         2.37707e+02         2.32366e+02         2.44019e+02         2.24625e+02         2.32530e+02         2.32243e+02         2.43408e+02         2.77195e+02
3200.708000         2.32762e+02         2.34782e+02         2.30741e+02         2.31993e+02         2.33071e+02         2.36017e+02         2.23655e+02         2.34001e+02         2.32371e+02         2.40789e+02         2.68526e+02
3200.792000         2.30312e+02         2.30655e+02         2.29969e+02         2.25907e+02         2.32087e+02         2.29195e+02         2.19092e+02         2.31579e+02         2.32469e+02         2.36952e+02         2.79120e+02
3200.875000         2.28017e+02         2.26178e+02         2.29856e+02         2.19732e+02         2.31355e+02         2.20911e+02         2.17287e+02         2.29508e+02         2.32746e+02         2.34002e+02         2.71070e+02
3200.958000         2.27306e+02         2.22607e+02         2.32005e+02         2.17216e+02         2.31371e+02         2.15822e+02         2.20105e+02         2.26897e+02         2.34741e+02         2.30984e+02         2.65973e+02
3201.042000         2.28156e+02         2.24353e+02         2.31958e+02         2.16578e+02         2.32820e+02         2.14130e+02         2.21652e+02         2.30818e+02         2.34328e+02         2.30747e+02         2.66729e+02
3201.125000         2.27450e+02         2.24430e+02         2.30470e+02         2.13479e+02         2.33078e+02         2.13569e+02         2.13292e+02         2.31297e+02         2.34419e+02         2.33211e+02         2.64316e+02
3201.208000         2.27745e+02         2.26069e+02         2.29420e+02         2.17071e+02         2.32044e+02         2.19503e+02         2.12031e+02         2.30221e+02         2.33418e+02         2.34117e+02         2.60261e+02
3201.292000         2.28295e+02         2.28989e+02         2.27601e+02         2.22152e+02         2.30770e+02         2.24639e+02         2.16998e+02         2.31740e+02         2.30039e+02         2.34148e+02         2.47277e+02
3201.375000         2.29409e+02         2.31229e+02         2.27588e+02         2.28198e+02         2.29896e+02         2.32997e+02         2.18253e+02         2.30112e+02         2.29734e+02         2.35355e+02         2.54949e+02
3201.458000         2.33636e+02         2.36947e+02         2.30325e+02         2.39001e+02         2.31475e+02         2.46739e+02         2.22965e+02         2.30755e+02         2.32018e+02         2.37328e+02         2.56547e+02
3201.542000         2.35056e+02         2.39586e+02         2.30525e+02         2.39892e+02         2.33108e+02         2.46219e+02         2.26782e+02         2.35393e+02         2.31386e+02         2.42202e+02         2.57287e+02
3201.625000         2.34623e+02         2.38624e+02         2.30621e+02         2.38289e+02         2.33146e+02         2.43652e+02         2.27175e+02         2.35445e+02         2.31414e+02         2.46034e+02         2.67952e+02
3201.708000         2.33831e+02         2.36448e+02         2.31214e+02         2.35523e+02         2.33150e+02         2.40305e+02         2.25613e+02         2.34009e+02         2.32502e+02         2.42960e+02         2.73377e+02
3201.792000         2.30725e+02         2.30997e+02         2.30453e+02         2.26726e+02         2.32336e+02         2.29811e+02         2.20333e+02         2.31747e+02         2.32780e+02         2.35715e+02         2.72268e+02
3201.875000         2.27297e+02         2.25465e+02         2.29130e+02         2.17726e+02         2.31153e+02         2.18827e+02         2.15443e+02         2.29663e+02         2.32276e+02         2.35421e+02         2.64781e+02
3201.958000         2.27681e+02         2.24124e+02         2.31239e+02         2.16715e+02         2.32099e+02         2.15312e+02         2.19624e+02         2.29696e+02         2.33910e+02         2.36139e+02         2.72849e+02
3202.042000         2.28775e+02         2.24969e+02         2.32580e+02         2.17250e+02         2.33417e+02         2.14889e+02         2.22142e+02         2.31343e+02         2.34980e+02         2.36554e+02         2.77706e+02
3202.125000         2.28210e+02         2.26142e+02         2.30278e+02         2.17153e+02         2.32664e+02         2.18182e+02         2.15021e+02         2.31174e+02         2.33786e+02         2.32801e+02         2.69121e+02
3202.208000         2.28662e+02         2.27111e+02         2.30213e+02         2.18352e+02         2.32814e+02         2.19284e+02         2.16421e+02         2.32060e+02         2.33383e+02         2.33212e+02         2.66115e+02
3202.292000         2.28454e+02         2.28782e+02         2.28126e+02         2.20743e+02         2.31560e+02         2.23870e+02         2.14261e+02         2.31887e+02         2.31314e+02         2.36788e+02         2.61210e+02
3202.375000         2.29687e+02         2.31640e+02         2.27734e+02         2.27399e+02         2.30608e+02         2.32703e+02         2.16406e+02         2.30967e+02         2.30338e+02         2.32749e+02         2.55309e+02
3202.458000         2.33021e+02         2.35616e+02         2.30427e+02         2.37937e+02         2.31041e+02         2.44410e+02         2.24522e+02         2.30055e+02         2.31784e+02         2.36206e+02         2.61591e+02
3202.542000         2.35050e+02         2.39530e+02         2.30570e+02         2.41832e+02         2.32318e+02         2.48399e+02         2.28222e+02         2.33921e+02         2.31110e+02         2.40519e+02         2.67482e+02
3202.625000         2.34014e+02         2.37736e+02         2.30291e+02         2.37526e+02         2.32599e+02         2.42089e+02         2.28069e+02         2.34984e+02         2.30802e+02         2.43395e+02         2.71662e+02
3202.708000         2.33304e+02         2.35552e+02         2.31056e+02         2.33296e+02         2.33308e+02         2.37300e+02         2.24999e+02         2.34447e+02         2.32449e+02         2.39726e+02         2.77903e+02
3202.792000         2.29277e+02         2.29064e+02         2.29489e+02         2.24603e+02         2.31159e+02         2.28134e+02         2.17285e+02         2.29652e+02         2.32295e+02         2.37313e+02         2.77634e+02
3202.875000         2.28229e+02         2.26666e+02         2.29792e+02         2.19077e+02         2.31915e+02         2.19375e+02         2.18460e+02         2.31276e+02         2.32397e+02         2.36514e+02         2.78656e+02
3202.958000         2.28213e+02         2.24237e+02         2.32188e+02         2.17365e+02         2.32582e+02         2.15361e+02         2.21517e+02         2.29849e+02         2.34642e+02         2.32958e+02         2.73074e+02
